package com.paulharding.spacex.testDouble.spy;

import com.paulharding.spacex.core.interactor.RetrieveRocketInteractor;
import com.paulharding.spacex.core.model.rocket.Rocket;

import java.util.List;

public class RetrieveRocketInteractorSpy implements RetrieveRocketInteractor {

    private int timesRetrieveAllCalled = 0;

    public boolean retrieveAllWasCalled() {
        return timesRetrieveAllCalled > 0;
    }

    @Override
    public List<Rocket> retrieveAll() {
        timesRetrieveAllCalled++;
        return null;
    }

}
