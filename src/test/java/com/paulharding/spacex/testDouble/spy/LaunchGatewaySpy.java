package com.paulharding.spacex.testDouble.spy;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.core.model.launch.Launch;

import java.util.List;

public class LaunchGatewaySpy implements LaunchGateway {

    private int timesRetrieveCalled = 0;

    @Override
    public List<Launch> retrieveRocketLaunches(String rocketId) {
        timesRetrieveCalled++;
        return null;
    }

    public boolean retrieveWasCalled() {
        return timesRetrieveCalled > 0;
    }

}
