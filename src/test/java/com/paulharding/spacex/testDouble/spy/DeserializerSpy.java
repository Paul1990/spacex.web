package com.paulharding.spacex.testDouble.spy;

import com.paulharding.spacex.gateway.api.deserialize.Deserializer;

public class DeserializerSpy implements Deserializer {

    private int timesDeserializeCalled = 0;

    @Override
    public <T> T deserialize(String json, Class<T> resultClass) {
        timesDeserializeCalled++;
        return null;
    }

    public boolean deserializeWasCalled() {
        return timesDeserializeCalled > 0;
    }

}
