package com.paulharding.spacex.testDouble.spy;

import com.paulharding.spacex.core.interactor.RetrieveLaunchInteractor;
import com.paulharding.spacex.core.model.launch.Launch;

import java.util.List;

public class RetrieveLaunchInteractorSpy implements RetrieveLaunchInteractor {

    private int timesRetrieveAllCalled = 0;

    @Override
    public List<Launch> retrieveRocketLaunches(String rocketId) {
        timesRetrieveAllCalled++;
        return null;
    }

    public boolean retrieveRocketLaunchesWasCalled() {
        return timesRetrieveAllCalled > 0;
    }

}
