package com.paulharding.spacex.testDouble.spy;

import com.paulharding.spacex.gateway.api.http.HttpRequester;

import java.util.ArrayList;
import java.util.List;

public class HttpRequesterSpy implements HttpRequester {

    private List<String> makeRequestParameters = new ArrayList<>();

    @Override
    public String makeRequest(String url) {
        makeRequestParameters.add(url);
        return null;
    }

    public boolean makeRequestWasCalled() {
        return makeRequestParameters.size() > 0;
    }

    public boolean makeRequestWasCalledWith(String url) {
        return makeRequestParameters.contains(url);
    }

}
