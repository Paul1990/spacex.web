package com.paulharding.spacex.testDouble.spy;

import com.paulharding.spacex.core.model.launch.Launch;
import com.paulharding.spacex.gateway.api.conversion.Converter;
import com.paulharding.spacex.gateway.api.model.response.retrieveLaunches.LaunchApiResponse;

public class LaunchConverterSpy implements Converter<LaunchApiResponse, Launch> {

    private int timesConvertCalled = 0;

    @Override
    public Launch convert(LaunchApiResponse rocketApiResponse) {
        timesConvertCalled++;
        return null;
    }

    public boolean convertWasCalled() {
        return timesConvertCalled > 0;
    }

}
