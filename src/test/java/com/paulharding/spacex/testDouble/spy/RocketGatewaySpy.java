package com.paulharding.spacex.testDouble.spy;

import com.paulharding.spacex.core.model.rocket.Rocket;
import com.paulharding.spacex.core.gateway.RocketGateway;

import java.util.List;

public class RocketGatewaySpy implements RocketGateway {

    private int timesRetrieveAllCalled = 0;

    @Override
    public List<Rocket> retrieveAll() {
        timesRetrieveAllCalled++;
        return null;
    }


    public boolean retrieveAllWasCalled() {
        return timesRetrieveAllCalled > 0;
    }
}
