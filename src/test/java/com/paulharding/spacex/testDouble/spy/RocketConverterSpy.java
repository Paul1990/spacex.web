package com.paulharding.spacex.testDouble.spy;

import com.paulharding.spacex.core.model.rocket.Rocket;
import com.paulharding.spacex.gateway.api.conversion.Converter;
import com.paulharding.spacex.gateway.api.model.response.retrieveRockets.RocketApiResponse;

public class RocketConverterSpy implements Converter<RocketApiResponse, Rocket> {

    private int timesConvertCalled = 0;

    @Override
    public Rocket convert(RocketApiResponse rocketApiResponse) {
        timesConvertCalled++;
        return null;
    }

    public boolean convertWasCalled() {
        return timesConvertCalled > 0;
    }

}
