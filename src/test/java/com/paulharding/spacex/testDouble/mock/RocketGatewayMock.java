package com.paulharding.spacex.testDouble.mock;

import com.paulharding.spacex.core.model.rocket.Rocket;
import com.paulharding.spacex.testDouble.spy.RocketGatewaySpy;

import java.util.List;

public class RocketGatewayMock extends RocketGatewaySpy {

    private RocketAccessException exception;

    @Override
    public List<Rocket> retrieveAll() {
        List<Rocket> result = super.retrieveAll();

        if (exception != null)
            throw exception;

        return result;

    }

    public void setException(RocketAccessException exception) {
        this.exception = exception;
    }

}
