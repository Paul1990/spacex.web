package com.paulharding.spacex.testDouble.mock;

import com.paulharding.spacex.core.model.rocket.Rocket;
import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.testDouble.spy.RetrieveRocketInteractorSpy;

import java.util.List;

public class RetrieveRocketInteractorMock extends RetrieveRocketInteractorSpy {

    private RocketGateway.RocketAccessException exception;

    @Override
    public List<Rocket> retrieveAll() {

        List<Rocket> rockets = super.retrieveAll();

        if (exception != null)
            throw exception;

        return rockets;

    }

    public void setException(RocketGateway.RocketAccessException exception) {
        this.exception = exception;
    }

}
