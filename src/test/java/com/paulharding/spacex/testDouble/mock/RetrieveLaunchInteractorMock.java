package com.paulharding.spacex.testDouble.mock;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.core.model.launch.Launch;
import com.paulharding.spacex.testDouble.spy.RetrieveLaunchInteractorSpy;

import java.util.List;

public class RetrieveLaunchInteractorMock extends RetrieveLaunchInteractorSpy {

    private LaunchGateway.LaunchAccessException exception;

    @Override
    public List<Launch> retrieveRocketLaunches(String rocketId) {

        List<Launch> rockets = super.retrieveRocketLaunches(rocketId);

        if (exception != null)
            throw exception;

        return rockets;

    }

    public void setException(LaunchGateway.LaunchAccessException exception) {
        this.exception = exception;
    }

}
