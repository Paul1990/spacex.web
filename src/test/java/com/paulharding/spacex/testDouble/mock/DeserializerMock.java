package com.paulharding.spacex.testDouble.mock;

import com.paulharding.spacex.testDouble.spy.DeserializerSpy;

public class DeserializerMock<T> extends DeserializerSpy {

    private T deserializeReturn;
    private FailedDeserializationException deserializeException;

    @Override
    public <T> T deserialize(String json, Class<T> resultClass) {

        super.deserialize(json, resultClass);

        if (deserializeException != null)
            throw deserializeException;

        return (T) deserializeReturn;

    }

    public void setDeserializeReturn(T deserializeReturn) {
        this.deserializeReturn = deserializeReturn;
    }

    public void setDeserializeException(FailedDeserializationException deserializeException) {
        this.deserializeException = deserializeException;
    }
}
