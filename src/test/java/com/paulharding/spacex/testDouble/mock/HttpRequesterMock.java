package com.paulharding.spacex.testDouble.mock;

import com.paulharding.spacex.testDouble.spy.HttpRequesterSpy;

public class HttpRequesterMock extends HttpRequesterSpy {

    private String makeRequestJson;
    private FailedHttpRequestException exception;

    @Override
    public String makeRequest(String url) {

        super.makeRequest(url);

        if (exception != null)
            throw exception;

        return makeRequestJson;

    }

    public void setMakeRequestJson(String makeRequestJson) {
        this.makeRequestJson = makeRequestJson;
    }

    public void setException(FailedHttpRequestException exception) {
        this.exception = exception;
    }

}
