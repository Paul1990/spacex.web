package com.paulharding.spacex.testDouble.mock;

import com.paulharding.spacex.core.model.launch.Launch;
import com.paulharding.spacex.testDouble.spy.LaunchGatewaySpy;

import java.util.List;

public class LaunchGatewayMock extends LaunchGatewaySpy {

    private LaunchAccessException exception;

    @Override
    public List<Launch> retrieveRocketLaunches(String rocketId) {

        List<Launch> result = super.retrieveRocketLaunches(rocketId);

        if (exception != null)
            throw exception;

        return result;

    }

    public void setException(LaunchAccessException exception) {
        this.exception = exception;
    }

}
