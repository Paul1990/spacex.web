package com.paulharding.spacex.web.controller;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.testDouble.mock.RetrieveLaunchInteractorMock;
import com.paulharding.spacex.web.model.RetrieveLaunchResponse;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

public class LaunchControllerTest {

    private LaunchController controller;
    private RetrieveLaunchInteractorMock interactorMock;

    @Before
    public void setup() {
        interactorMock = new RetrieveLaunchInteractorMock();
        controller = new LaunchController(interactorMock);
    }

    @Test
    public void retrieveRocketLaunches_callsInteractor() {

        // When
        controller.retrieveRocketLaunches("");

        // Then
        assertTrue(interactorMock.retrieveRocketLaunchesWasCalled());

    }

    @Test
    public void whenInteractorThrowsException_thenResponseContainsError() {

        // Given
        interactorMock.setException(new LaunchGateway.LaunchAccessException(""));

        // When
        RetrieveLaunchResponse response = controller.retrieveRocketLaunches("");

        // Then
        assertFalse(response.getErrors().isEmpty());

    }

    @Test
    public void whenInteractorRetrievesDataSuccessfully_thenThereAreNoErrorsInResponse() {

        // When
        RetrieveLaunchResponse response = controller.retrieveRocketLaunches("");

        // Then
        assertTrue(response.getErrors().isEmpty());

    }

}
