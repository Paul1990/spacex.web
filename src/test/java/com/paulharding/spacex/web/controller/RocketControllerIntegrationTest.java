package com.paulharding.spacex.web.controller;

import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.core.interactor.RetrieveRocketInteractor;
import com.paulharding.spacex.core.interactor.impl.RetrieveRocketInteractorImpl;
import com.paulharding.spacex.core.model.rocket.Rocket;
import com.paulharding.spacex.gateway.api.*;
import com.paulharding.spacex.gateway.api.conversion.Converter;
import com.paulharding.spacex.gateway.api.conversion.impl.RocketConverter;
import com.paulharding.spacex.gateway.api.deserialize.Deserializer;
import com.paulharding.spacex.gateway.api.deserialize.impl.JacksonDeserializer;
import com.paulharding.spacex.gateway.api.http.HttpRequester;
import com.paulharding.spacex.gateway.api.http.impl.JavaNetHttpRequester;
import com.paulharding.spacex.gateway.api.model.response.retrieveRockets.RocketApiResponse;
import com.paulharding.spacex.web.model.RetrieveRocketResponse;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RocketControllerIntegrationTest {

    private RocketController controller;

    @Before
    public void setup() {

        HttpRequester httpRequester = new JavaNetHttpRequester();
        Deserializer deserializer = new JacksonDeserializer();
        Converter<RocketApiResponse, Rocket> converter = new RocketConverter();
        RocketGateway gateway = new SpaceXApiRocketGateway(httpRequester, deserializer, converter);
        RetrieveRocketInteractor interactor = new RetrieveRocketInteractorImpl(gateway);

        this.controller = new RocketController(interactor);

    }

    @Ignore("Don't want to call the API every time the tests are run")
    @Test
    public void retrieveAll_retrievesRocketsSuccessfully() {

        // When
        RetrieveRocketResponse response = controller.retrieveAll();

        // Then
        assertTrue(response.getRockets().size() > 0);
        assertTrue(response.getRockets().size() > 0);

    }

}
