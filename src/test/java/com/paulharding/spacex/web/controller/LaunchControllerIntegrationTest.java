package com.paulharding.spacex.web.controller;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.core.interactor.RetrieveLaunchInteractor;
import com.paulharding.spacex.core.interactor.impl.RetrieveLaunchInteractorImpl;
import com.paulharding.spacex.core.model.launch.Launch;
import com.paulharding.spacex.gateway.api.*;
import com.paulharding.spacex.gateway.api.conversion.Converter;
import com.paulharding.spacex.gateway.api.conversion.impl.LaunchConverter;
import com.paulharding.spacex.gateway.api.deserialize.Deserializer;
import com.paulharding.spacex.gateway.api.deserialize.impl.JacksonDeserializer;
import com.paulharding.spacex.gateway.api.http.HttpRequester;
import com.paulharding.spacex.gateway.api.http.impl.JavaNetHttpRequester;
import com.paulharding.spacex.gateway.api.model.response.retrieveLaunches.LaunchApiResponse;
import com.paulharding.spacex.web.model.RetrieveLaunchResponse;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LaunchControllerIntegrationTest {

    private LaunchController controller;

    @Before
    public void setup() {

        HttpRequester httpRequester = new JavaNetHttpRequester();
        Deserializer deserializer = new JacksonDeserializer();
        Converter<LaunchApiResponse, Launch> converter = new LaunchConverter();
        LaunchGateway gateway = new SpaceXApiLaunchGateway(httpRequester, deserializer, converter);
        RetrieveLaunchInteractor interactor = new RetrieveLaunchInteractorImpl(gateway);

        this.controller = new LaunchController(interactor);

    }

    @Ignore("Don't want to call the API every time the tests are run")
    @Test
    public void retrieveRocketLaunches_retrievesRockets() {

        // Given
        String rocketId = "falcon9";

        // When
        RetrieveLaunchResponse response = controller.retrieveRocketLaunches(rocketId);

        // Then
        assertTrue(response.getLaunches().size() > 0);
        assertTrue(response.getLaunches().size() > 0);

    }

}
