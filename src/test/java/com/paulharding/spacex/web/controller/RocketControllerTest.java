package com.paulharding.spacex.web.controller;

import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.testDouble.mock.RetrieveRocketInteractorMock;
import com.paulharding.spacex.web.model.RetrieveRocketResponse;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RocketControllerTest {

    private RocketController rocketController;
    private RetrieveRocketInteractorMock interactorMock;

    @Before
    public void setup() {
        interactorMock = new RetrieveRocketInteractorMock();
        rocketController = new RocketController(interactorMock);
    }

    @Test
    public void retrieveAll_callsRocketRetrieverInteractor() {

        rocketController.retrieveAll();

        assertTrue(interactorMock.retrieveAllWasCalled());

    }

    @Test
    public void whenInteractorThrowsException_thenResponseContainsError() {

        // Given
        interactorMock.setException(new RocketGateway.RocketAccessException());

        // When
        RetrieveRocketResponse response = rocketController.retrieveAll();

        // Then
        assertFalse(response.getErrors().isEmpty());

    }

    @Test
    public void whenInteractorRetrievesDataSuccessfully_thenThereAreNoErrorsInResponse() {

        // When
        RetrieveRocketResponse response = rocketController.retrieveAll();

        // Then
        assertTrue(response.getErrors().isEmpty());

    }

}
