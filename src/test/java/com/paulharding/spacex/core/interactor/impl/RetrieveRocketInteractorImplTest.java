package com.paulharding.spacex.core.interactor.impl;

import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.core.interactor.impl.RetrieveRocketInteractorImpl;
import com.paulharding.spacex.testDouble.mock.RocketGatewayMock;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RetrieveRocketInteractorImplTest {

    private RetrieveRocketInteractorImpl interactor;
    private RocketGatewayMock gateway;

    @Before
    public void setup() {
        gateway = new RocketGatewayMock();
        interactor = new RetrieveRocketInteractorImpl(gateway);
    }

    @Test
    public void retrieveAll_callsRocketGateway() {

        // When
        interactor.retrieveAll();

        // Then
        assertTrue(gateway.retrieveAllWasCalled());

    }

    @Test(expected = RocketGateway.RocketAccessException.class)
    public void whenGatewayThrowsException_exceptionIsPassedToCaller() {

        // Given
        gateway.setException(new RocketGateway.RocketAccessException());

        // When
        interactor.retrieveAll();

    }

}
