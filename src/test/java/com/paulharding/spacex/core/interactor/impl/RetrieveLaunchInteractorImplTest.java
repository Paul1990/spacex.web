package com.paulharding.spacex.core.interactor.impl;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.core.interactor.impl.RetrieveLaunchInteractorImpl;
import com.paulharding.spacex.testDouble.mock.LaunchGatewayMock;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RetrieveLaunchInteractorImplTest {

    private RetrieveLaunchInteractorImpl interactor;
    private LaunchGatewayMock gatewayMock;

    @Before
    public void setup() {
        this.gatewayMock = new LaunchGatewayMock();
        this.interactor = new RetrieveLaunchInteractorImpl(gatewayMock);
    }

    @Test
    public void retrieveRocketLaunches_callsGateway() {

        // When
        interactor.retrieveRocketLaunches("");

        // Then
        assertTrue(gatewayMock.retrieveWasCalled());

    }

    @Test(expected = LaunchGateway.LaunchAccessException.class)
    public void whenGatewayThrows_thenExceptionIsPassedUp() {

        // Given
        gatewayMock.setException(new LaunchGateway.LaunchAccessException(""));

        // Then
        interactor.retrieveRocketLaunches("");

    }

}
