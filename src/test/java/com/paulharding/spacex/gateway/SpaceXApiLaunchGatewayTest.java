package com.paulharding.spacex.gateway;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.gateway.api.deserialize.Deserializer;
import com.paulharding.spacex.gateway.api.http.HttpRequester;
import com.paulharding.spacex.gateway.api.SpaceXApiLaunchGateway;
import com.paulharding.spacex.gateway.api.model.response.retrieveLaunches.LaunchApiResponse;
import com.paulharding.spacex.testDouble.mock.DeserializerMock;
import com.paulharding.spacex.testDouble.mock.HttpRequesterMock;
import com.paulharding.spacex.testDouble.spy.LaunchConverterSpy;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SpaceXApiLaunchGatewayTest {

    private SpaceXApiLaunchGateway gateway;

    private HttpRequesterMock httpRequesterMock;
    private DeserializerMock<LaunchApiResponse[]> deserializerMock;
    private LaunchConverterSpy launchConverterSpy;

    @Before
    public void setup() {

        this.httpRequesterMock = new HttpRequesterMock();
        httpRequesterMock.setMakeRequestJson("[]");

        this.deserializerMock = new DeserializerMock<>();
        deserializerMock.setDeserializeReturn(new LaunchApiResponse[0]);

        this.launchConverterSpy = new LaunchConverterSpy();
        this.gateway = new SpaceXApiLaunchGateway(httpRequesterMock, deserializerMock, launchConverterSpy);
    }

    @Test
    public void retrieveRocketLaunches_callsHttpRequester() {

        // Given
        String mockRocketId = "rocketid";
        String expectedRequestUrl = "https://api.spacexdata.com/v3/launches?rocket_id=" + mockRocketId;

        // When
        gateway.retrieveRocketLaunches(mockRocketId);

        // Then
        assertTrue(httpRequesterMock.makeRequestWasCalled());
        assertTrue(httpRequesterMock.makeRequestWasCalledWith(expectedRequestUrl));

    }

    @Test(expected = LaunchGateway.LaunchAccessException.class)
    public void whenHttpRequesterThrows_thenLaunchAccessExceptionIsThrown() {

        // Given
        httpRequesterMock.setException(new HttpRequester.FailedHttpRequestException());

        // When
        gateway.retrieveRocketLaunches("");

    }

    @Test
    public void retrieveRocketLaunches_callsDeserializer() {

        // When
        gateway.retrieveRocketLaunches("");

        // Then
        assertTrue(deserializerMock.deserializeWasCalled());

    }

    @Test(expected = LaunchGateway.LaunchAccessException.class)
    public void whenDeserializerThrows_thenLaunchAccessExceptionIsThrown() {

        // Given
        deserializerMock.setDeserializeException(new Deserializer.FailedDeserializationException());

        // When
        gateway.retrieveRocketLaunches("");

    }

    @Test
    public void retrieveRocketLaunches_callsConverter() {

        // Given
        LaunchApiResponse[] apiResponse = new LaunchApiResponse[1];
        apiResponse[0] = new LaunchApiResponse();
        deserializerMock.setDeserializeReturn(apiResponse);

        // When
        gateway.retrieveRocketLaunches("");

        // Then
        assertTrue(launchConverterSpy.convertWasCalled());

    }

}
