package com.paulharding.spacex.gateway;

import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.gateway.api.deserialize.Deserializer;
import com.paulharding.spacex.gateway.api.http.HttpRequester;
import com.paulharding.spacex.gateway.api.SpaceXApiRocketGateway;
import com.paulharding.spacex.gateway.api.model.response.retrieveRockets.RocketApiResponse;
import com.paulharding.spacex.testDouble.mock.DeserializerMock;
import com.paulharding.spacex.testDouble.mock.HttpRequesterMock;
import com.paulharding.spacex.testDouble.spy.RocketConverterSpy;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SpaceXApiRocketGatewayTest {

    private SpaceXApiRocketGateway gateway;
    private HttpRequesterMock httpRequesterMock;
    private DeserializerMock<RocketApiResponse[]> deserializerMock;
    private RocketConverterSpy converterSpy;

    private static final String RETRIEVE_ROCKET_URL = "https://api.spacexdata.com/v3/rockets";

    @Before
    public void setup() {

        httpRequesterMock = new HttpRequesterMock();
        httpRequesterMock.setMakeRequestJson("[]");

        deserializerMock = new DeserializerMock<>();
        deserializerMock.setDeserializeReturn(new RocketApiResponse[0]);

        converterSpy = new RocketConverterSpy();

        gateway = new SpaceXApiRocketGateway(httpRequesterMock, deserializerMock, converterSpy);

    }

    @Test
    public void retrieveAll_callshttpRequester() {

        // When
        gateway.retrieveAll();

        // Then
        assertTrue(httpRequesterMock.makeRequestWasCalled());
        assertTrue(httpRequesterMock.makeRequestWasCalledWith(RETRIEVE_ROCKET_URL));

    }

    @Test(expected = RocketGateway.RocketAccessException.class)
    public void whenhttpRequesterThrows_thenRocketAccessExceptionIsThrown() {

        // Given
        httpRequesterMock.setException(new HttpRequester.FailedHttpRequestException());

        // When
        gateway.retrieveAll();

    }

    @Test
    public void retrieveAll_callsDeserializer() {

        // When
        gateway.retrieveAll();

        // Then
        assertTrue(deserializerMock.deserializeWasCalled());

    }

    @Test(expected = RocketGateway.RocketAccessException.class)
    public void whenDeserializerThrows_thenRocketAccessExceptionIsThrown() {

        // Given
        deserializerMock.setDeserializeException(new Deserializer.FailedDeserializationException());

        // When
        gateway.retrieveAll();

    }

    @Test
    public void retrieveAll_callsConverter() {

        // Given
        RocketApiResponse[] apiResponse = new RocketApiResponse[1];
        apiResponse[0] = new RocketApiResponse();
        deserializerMock.setDeserializeReturn(apiResponse);

        // When
        gateway.retrieveAll();

        // Then
        assertTrue(converterSpy.convertWasCalled());

    }

}
