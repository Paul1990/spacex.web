package com.paulharding.spacex.gateway.api.conversion.impl;

import com.paulharding.spacex.core.model.launch.*;
import com.paulharding.spacex.gateway.api.conversion.Converter;
import com.paulharding.spacex.gateway.api.model.response.retrieveLaunches.*;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LaunchConverter implements Converter<LaunchApiResponse, Launch> {

    @Override
    public Launch convert(LaunchApiResponse source) {

        return new Launch(
            source.getFlight_number(),
            source.getMission_name(),
            source.getMission_id(),
            source.isUpcoming(),
            source.getLaunch_year(),
            source.getLaunch_date_unix(),
            source.getLaunch_date_utc(),
            source.getLaunch_date_local(),
            source.isIs_tentative(),
            source.getTentative_max_precision(),
            source.isTbd(),
            source.getLaunch_window(),
            convert(source.getRocket()),
            source.getShips(),
            convert(source.getTelemetry()),
            convert(source.getLaunch_site()),
            source.isLaunch_success(),
            convert(source.getLaunch_failure_details()),
            convert(source.getLinks()),
            source.getDetails(),
            source.getStatic_fire_date_utc(),
            source.getStatic_fire_date_unix(),
            convert(source.getTimeline())
        );

    }

    private Rocket convert(RocketApiResponse source) {

        if (source == null)
            return null;

        return new Rocket(
            source.getRocket_id(),
            source.getRocket_name(),
            source.getRocket_type(),
            convert(source.getFirst_stage()),
            convert(source.getSecond_stage()),
            convert(source.getFairings())
        );

    }

    private FirstStage convert(FirstStageApiResponse source) {

        if (source == null)
            return null;

        return new FirstStage(
            convert(source.getCores())
        );

    }

    private Core[] convert(CoreApiResponse[] source) {

        if (source == null)
            return null;

        return Stream.of(source)
                .map(this::convert)
                .collect(Collectors.toList())
                .toArray(new Core[source.length]);

    }

    private Core convert(CoreApiResponse source) {

        if (source == null)
            return null;

        return new Core(
            source.getCore_serial(),
            source.getFlight(),
            source.getBlock(),
            source.isGridfins(),
            source.isLegs(),
            source.isReused(),
            source.isLand_success(),
            source.isLanding_intent(),
            source.getLanding_type(),
            source.getLanding_vehicle()
        );

    }

    private SecondStage convert(SecondStageApiResponse source) {

        if (source == null)
            return null;

        return new SecondStage(
            source.getBlock(),
            convert(source.getPayloads())
        );

    }

    private Payload[] convert(PayloadApiResponse[] source) {

        if (source == null)
            return null;

        return Stream.of(source)
                .map(this::convert)
                .collect(Collectors.toList())
                .toArray(new Payload[source.length]);

    }

    private Payload convert(PayloadApiResponse source) {

        if (source == null)
            return null;

        return new Payload(
            source.getPayload_id(),
            source.getNorad_id(),
            source.getCap_serial(),
            source.isReused(),
            source.getCustomers(),
            source.getNationality(),
            source.getManufacturer(),
            source.getPayload_type(),
            source.getPayload_mass_kg(),
            source.getPayload_mass_lbs(),
            source.getOrbit(),
            convert(source.getOrbit_params()),
            source.getMass_returned_kg(),
            source.getMass_returned_lbs(),
            source.getFlight_time_sec(), source.getCargo_manifest()
        );

    }

    private Orbit convert(OrbitParamApiResponse source) {

        if (source == null)
            return null;

        return new Orbit(
            source.getReference_system(),
            source.getRegime(),
            source.getLongitude(),
            source.getSemi_major_axis_km(),
            source.getEccentricity(),
            source.getPeriapsis_km(),
            source.getApoapsis_km(),
            source.getInclination_deg(),
            source.getPeriod_min(),
            source.getLifespan_years(),
            source.getEpoch(),
            source.getMean_motion(),
            source.getRaan(),
            source.getArg_of_pericenter(),
            source.getMean_anomaly()
        );

    }

    private Fairing convert(FairingApiResponse source) {

        if (source == null)
            return null;

        return new Fairing(
           source.isReused(),
           source.isRecovery_attempt(),
           source.isRecovered(),
           source.getShip()
        );

    }

    private Telemetry convert(TelemetryApiResponse source) {

        if (source == null)
            return null;

        return new Telemetry(
             source.getFlight_club()
        );

    }

    private LaunchSite convert(LaunchSiteApiResponse source) {

        if (source == null)
            return null;

        return new LaunchSite(
            source.getSite_id(),
            source.getSite_name(),
            source.getSite_name_long()
        );

    }

    private LaunchFailureDetail convert(LaunchFailureDetailApiResponse source) {

        if (source == null)
            return null;

        return new LaunchFailureDetail(
            source.getTime(),
            source.getAltitude(),
            source.getReason()
        );

    }

    private LaunchLink convert(LaunchLinkApiResponse source) {

        if (source == null)
            return null;

        return new LaunchLink(
            source.getMission_patch(),
            source.getMission_patch_small(),
            source.getReddit_campaign(),
            source.getReddit_launch(),
            source.getReddit_recovery(),
            source.getReddit_media(),
            source.getPresskit(),
            source.getArticle_link(),
            source.getWikipedia(),
            source.getVideo_link(),
            source.getYoutube_id(),
            source.getFlickr_images()
        );

    }

    private Timeline convert(TimelineApiResponse source) {

        if (source == null)
            return null;

        return new Timeline(
            source.getWebcast_liftoff(),
            source.getWebcast_launch(),
            source.getGo_for_prop_loading(),
            source.getRp1_loading(),
            source.getStage1_lox_loading(),
            source.getStage2_lox_loading(),
            source.getEngine_chill(),
            source.getPrelaunch_checks(),
            source.getPropellant_pressurization(),
            source.getGo_for_launch(),
            source.getIgnition(),
            source.getLiftoff(),
            source.getMaxq(),
            source.getBeco(),
            source.getSide_core_sep(),
            source.getSide_core_boostback(),
            source.getMeco(),
            source.getStage_sep(),
            source.getCenter_stage_sep(),
            source.getSecond_stage_ignition(),
            source.getCenter_core_boostback(),
            source.getFairing_deploy(),
            source.getSide_core_entry_burn(),
            source.getCenter_core_entry_burn(),
            source.getSide_core_landing(),
            source.getCenter_core_landing(),
            source.getFirst_stage_boostback_burn(),
            source.getFirst_stage_entry_burn(),
            source.getSeco1(),
            source.getSecond_stage_restart(),
            source.getSeco2(),
            source.getFirst_stage_landing_burn(),
            source.getFirst_stage_landing(),
            source.getPayload_deploy(),
            source.getPayload_deploy_1(),
            source.getPayload_deploy_2(),
            source.getDragon_separation(),
            source.getDragon_solar_deploy(),
            source.getDragon_bay_door_deploy()
        );

    }

}
