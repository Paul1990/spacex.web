package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class TelemetryApiResponse {

    private String flight_club;

    public String getFlight_club() {
        return flight_club;
    }

    public void setFlight_club(String flight_club) {
        this.flight_club = flight_club;
    }

}
