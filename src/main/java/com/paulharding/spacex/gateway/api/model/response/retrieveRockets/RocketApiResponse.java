package com.paulharding.spacex.gateway.api.model.response.retrieveRockets;

import java.util.Calendar;

public class RocketApiResponse {

    private long id;
    private boolean isActive;
    private int stages;
    private int boosters;
    private int cost_per_launch; // this name matches the property as it is in the response JSON
    private int success_rate_pct;
    private Calendar first_flight;
    private String country;
    private String company;
    private DistanceMeasurementApiResponse height;
    private DistanceMeasurementApiResponse diameter;
    private MassMeasurementApiResponse mass;
    private PayloadWeightApiResponse[] payload_weights;
    private FirstStageApiResponse first_stage;
    private SecondStageApiResponse second_stage;
    private EngineApiResponse engines;
    private LandingLegApiResponse landing_legs;
    private String[] flickr_images;
    private String wikipedia;
    private String description;
    private String rocket_id;
    private String rocket_name;
    private String rocket_type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getStages() {
        return stages;
    }

    public void setStages(int stages) {
        this.stages = stages;
    }

    public int getBoosters() {
        return boosters;
    }

    public void setBoosters(int boosters) {
        this.boosters = boosters;
    }

    public int getCost_per_launch() {
        return cost_per_launch;
    }

    public void setCost_per_launch(int costPerLaunch) {
        this.cost_per_launch = costPerLaunch;
    }

    public int getSuccess_rate_pct() {
        return success_rate_pct;
    }

    public void setSuccess_rate_pct(int success_rate_pct) {
        this.success_rate_pct = success_rate_pct;
    }

    public Calendar getFirst_flight() {
        return first_flight;
    }

    public void setFirst_flight(Calendar first_flight) {
        this.first_flight = first_flight;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public DistanceMeasurementApiResponse getHeight() {
        return height;
    }

    public void setHeight(DistanceMeasurementApiResponse height) {
        this.height = height;
    }

    public DistanceMeasurementApiResponse getDiameter() {
        return diameter;
    }

    public void setDiameter(DistanceMeasurementApiResponse diameter) {
        this.diameter = diameter;
    }

    public MassMeasurementApiResponse getMass() {
        return mass;
    }

    public void setMass(MassMeasurementApiResponse mass) {
        this.mass = mass;
    }

    public PayloadWeightApiResponse[] getPayload_weights() {
        return payload_weights;
    }

    public void setPayload_weights(PayloadWeightApiResponse[] payload_weights) {
        this.payload_weights = payload_weights;
    }

    public FirstStageApiResponse getFirst_stage() {
        return first_stage;
    }

    public void setFirst_stage(FirstStageApiResponse first_stage) {
        this.first_stage = first_stage;
    }

    public SecondStageApiResponse getSecond_stage() {
        return second_stage;
    }

    public void setSecond_stage(SecondStageApiResponse second_stage) {
        this.second_stage = second_stage;
    }

    public EngineApiResponse getEngines() {
        return engines;
    }

    public void setEngines(EngineApiResponse engines) {
        this.engines = engines;
    }

    public LandingLegApiResponse getLanding_legs() {
        return landing_legs;
    }

    public void setLanding_legs(LandingLegApiResponse landing_legs) {
        this.landing_legs = landing_legs;
    }

    public String[] getFlickr_images() {
        return flickr_images;
    }

    public void setFlickr_images(String[] flickr_images) {
        this.flickr_images = flickr_images;
    }

    public String getWikipedia() {
        return wikipedia;
    }

    public void setWikipedia(String wikipedia) {
        this.wikipedia = wikipedia;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRocket_id() {
        return rocket_id;
    }

    public void setRocket_id(String rocket_id) {
        this.rocket_id = rocket_id;
    }

    public String getRocket_name() {
        return rocket_name;
    }

    public void setRocket_name(String rocket_name) {
        this.rocket_name = rocket_name;
    }

    public String getRocket_type() {
        return rocket_type;
    }

    public void setRocket_type(String rocket_type) {
        this.rocket_type = rocket_type;
    }

}