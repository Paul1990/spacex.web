package com.paulharding.spacex.gateway.api.http;

public interface HttpRequester {

    /**
     * Makes a GET request to the given URL and returns the JSON
     *
     * @param url the request URL
     * @return the JSON response
     * @throws FailedHttpRequestException if the request fails
     */
    String makeRequest(String url);

    class FailedHttpRequestException extends RuntimeException {

        public FailedHttpRequestException() {}

        public FailedHttpRequestException(String message) {
            super(message);
        }

    }

}
