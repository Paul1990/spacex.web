package com.paulharding.spacex.gateway.api.deserialize.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paulharding.spacex.gateway.api.deserialize.Deserializer;

import java.io.IOException;

public class JacksonDeserializer implements Deserializer {

    @Override
    public <T> T deserialize(String json, Class<T> resultClass) {

        try {

            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, resultClass);

        } catch (IOException ex) {
            throw new FailedDeserializationException("Error deserializing " + json + " into type " + resultClass);
        }

    }

}
