package com.paulharding.spacex.gateway.api.http.impl;

import com.paulharding.spacex.gateway.api.http.HttpRequester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class JavaNetHttpRequester implements HttpRequester {

    @Override
    public String makeRequest(String url) {

        URL urlObj = createUrl(url);
        HttpURLConnection connection = createHttpUrlConnection(urlObj);

        return constructJson(connection);
    }


    private URL createUrl(String url) {

        try {
            return new URL(url);
        } catch (MalformedURLException ex) {
            throw new FailedHttpRequestException("URL is invalid");
        }

    }

    private HttpURLConnection createHttpUrlConnection(URL url) {

        try {

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            return connection;

        } catch (IOException ex) {
            throw new FailedHttpRequestException("Error opening the connection to make the request");
        }

    }

    private String constructJson(HttpURLConnection connection) {

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String readerLine;
            StringBuilder stringBuilder = new StringBuilder();

            while ( (readerLine = reader.readLine()) != null )
                stringBuilder.append(readerLine);

            reader.close();

            return stringBuilder.toString();

        } catch (IOException e) {
            throw new FailedHttpRequestException("Error reading data from the request");
        }

    }

}
