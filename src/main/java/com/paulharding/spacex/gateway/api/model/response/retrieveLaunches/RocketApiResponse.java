package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class RocketApiResponse {

    private String rocket_id;
    private String rocket_name;
    private String rocket_type;
    private FirstStageApiResponse first_stage;
    private SecondStageApiResponse second_stage;
    private FairingApiResponse fairings;

    public String getRocket_id() {
        return rocket_id;
    }

    public void setRocket_id(String rocket_id) {
        this.rocket_id = rocket_id;
    }

    public String getRocket_name() {
        return rocket_name;
    }

    public void setRocket_name(String rocket_name) {
        this.rocket_name = rocket_name;
    }

    public String getRocket_type() {
        return rocket_type;
    }

    public void setRocket_type(String rocket_type) {
        this.rocket_type = rocket_type;
    }

    public FirstStageApiResponse getFirst_stage() {
        return first_stage;
    }

    public void setFirst_stage(FirstStageApiResponse first_stage) {
        this.first_stage = first_stage;
    }

    public SecondStageApiResponse getSecond_stage() {
        return second_stage;
    }

    public void setSecond_stage(SecondStageApiResponse second_stage) {
        this.second_stage = second_stage;
    }

    public FairingApiResponse getFairings() {
        return fairings;
    }

    public void setFairings(FairingApiResponse fairings) {
        this.fairings = fairings;
    }

}
