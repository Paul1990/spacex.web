package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class OrbitParamApiResponse {

    private String reference_system;
    private String regime;
    private double longitude;
    private double semi_major_axis_km;
    private double eccentricity;
    private int periapsis_km;
    private int apoapsis_km;
    private int inclination_deg;
    private double period_min;
    private int lifespan_years;
    private String epoch;
    private double mean_motion;
    private double raan;
    private double arg_of_pericenter;
    private double mean_anomaly;

    public String getReference_system() {
        return reference_system;
    }

    public void setReference_system(String reference_system) {
        this.reference_system = reference_system;
    }

    public String getRegime() {
        return regime;
    }

    public void setRegime(String regime) {
        this.regime = regime;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSemi_major_axis_km() {
        return semi_major_axis_km;
    }

    public void setSemi_major_axis_km(double semi_major_axis_km) {
        this.semi_major_axis_km = semi_major_axis_km;
    }

    public double getEccentricity() {
        return eccentricity;
    }

    public void setEccentricity(double eccentricity) {
        this.eccentricity = eccentricity;
    }

    public int getPeriapsis_km() {
        return periapsis_km;
    }

    public void setPeriapsis_km(int periapsis_km) {
        this.periapsis_km = periapsis_km;
    }

    public int getApoapsis_km() {
        return apoapsis_km;
    }

    public void setApoapsis_km(int apoapsis_km) {
        this.apoapsis_km = apoapsis_km;
    }

    public int getInclination_deg() {
        return inclination_deg;
    }

    public void setInclination_deg(int inclination_deg) {
        this.inclination_deg = inclination_deg;
    }

    public double getPeriod_min() {
        return period_min;
    }

    public void setPeriod_min(double period_min) {
        this.period_min = period_min;
    }

    public int getLifespan_years() {
        return lifespan_years;
    }

    public void setLifespan_years(int lifespan_years) {
        this.lifespan_years = lifespan_years;
    }

    public String getEpoch() {
        return epoch;
    }

    public void setEpoch(String epoch) {
        this.epoch = epoch;
    }

    public double getMean_motion() {
        return mean_motion;
    }

    public void setMean_motion(double mean_motion) {
        this.mean_motion = mean_motion;
    }

    public double getRaan() {
        return raan;
    }

    public void setRaan(double raan) {
        this.raan = raan;
    }

    public double getArg_of_pericenter() {
        return arg_of_pericenter;
    }

    public void setArg_of_pericenter(double arg_of_pericenter) {
        this.arg_of_pericenter = arg_of_pericenter;
    }

    public double getMean_anomaly() {
        return mean_anomaly;
    }

    public void setMean_anomaly(double mean_anomaly) {
        this.mean_anomaly = mean_anomaly;
    }

}
