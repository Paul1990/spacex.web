package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TimelineApiResponse {

    private int webcast_liftoff;
    private int webcast_launch;
    private int go_for_prop_loading;
    private int rp1_loading;
    private int stage1_lox_loading;
    private int stage2_lox_loading;
    private int engine_chill;
    private int prelaunch_checks;
    private int propellant_pressurization;
    private int go_for_launch;
    private int ignition;
    private int liftoff;
    private int maxq;
    private int beco;
    private int side_core_sep;
    private int side_core_boostback;
    private int meco;
    private int stage_sep;
    private int center_stage_sep;
    private int second_stage_ignition;
    private int center_core_boostback;
    private int fairing_deploy;
    private int side_core_entry_burn;
    private int center_core_entry_burn;
    private int side_core_landing;
    private int center_core_landing;
    private int first_stage_boostback_burn;
    private int first_stage_entry_burn;
    private int seco1;
    private int second_stage_restart;
    private int seco2;
    private int first_stage_landing_burn;
    private int first_stage_landing;
    private int payload_deploy;
    private int payload_deploy_1;
    private int payload_deploy_2;
    private int dragon_separation;
    private int dragon_solar_deploy;
    private int dragon_bay_door_deploy;

    public int getWebcast_liftoff() {
        return webcast_liftoff;
    }

    public void setWebcast_liftoff(int webcast_liftoff) {
        this.webcast_liftoff = webcast_liftoff;
    }

    public int getWebcast_launch() {
        return webcast_launch;
    }

    public void setWebcast_launch(int webcast_launch) {
        this.webcast_launch = webcast_launch;
    }

    public int getGo_for_prop_loading() {
        return go_for_prop_loading;
    }

    public void setGo_for_prop_loading(int go_for_prop_loading) {
        this.go_for_prop_loading = go_for_prop_loading;
    }

    public int getRp1_loading() {
        return rp1_loading;
    }

    public void setRp1_loading(int rp1_loading) {
        this.rp1_loading = rp1_loading;
    }

    public int getStage1_lox_loading() {
        return stage1_lox_loading;
    }

    public void setStage1_lox_loading(int stage1_lox_loading) {
        this.stage1_lox_loading = stage1_lox_loading;
    }

    public int getStage2_lox_loading() {
        return stage2_lox_loading;
    }

    public void setStage2_lox_loading(int stage2_lox_loading) {
        this.stage2_lox_loading = stage2_lox_loading;
    }

    public int getEngine_chill() {
        return engine_chill;
    }

    public void setEngine_chill(int engine_chill) {
        this.engine_chill = engine_chill;
    }

    public int getPrelaunch_checks() {
        return prelaunch_checks;
    }

    public void setPrelaunch_checks(int prelaunch_checks) {
        this.prelaunch_checks = prelaunch_checks;
    }

    public int getPropellant_pressurization() {
        return propellant_pressurization;
    }

    public void setPropellant_pressurization(int propellant_pressurization) {
        this.propellant_pressurization = propellant_pressurization;
    }

    public int getGo_for_launch() {
        return go_for_launch;
    }

    public void setGo_for_launch(int go_for_launch) {
        this.go_for_launch = go_for_launch;
    }

    public int getIgnition() {
        return ignition;
    }

    public void setIgnition(int ignition) {
        this.ignition = ignition;
    }

    public int getLiftoff() {
        return liftoff;
    }

    public void setLiftoff(int liftoff) {
        this.liftoff = liftoff;
    }

    public int getMaxq() {
        return maxq;
    }

    public void setMaxq(int maxq) {
        this.maxq = maxq;
    }

    public int getBeco() {
        return beco;
    }

    public void setBeco(int beco) {
        this.beco = beco;
    }

    public int getSide_core_sep() {
        return side_core_sep;
    }

    public void setSide_core_sep(int side_core_sep) {
        this.side_core_sep = side_core_sep;
    }

    public int getSide_core_boostback() {
        return side_core_boostback;
    }

    public void setSide_core_boostback(int side_core_boostback) {
        this.side_core_boostback = side_core_boostback;
    }

    public int getMeco() {
        return meco;
    }

    public void setMeco(int meco) {
        this.meco = meco;
    }

    public int getStage_sep() {
        return stage_sep;
    }

    public void setStage_sep(int stage_sep) {
        this.stage_sep = stage_sep;
    }

    public int getCenter_stage_sep() {
        return center_stage_sep;
    }

    public void setCenter_stage_sep(int center_stage_sep) {
        this.center_stage_sep = center_stage_sep;
    }

    public int getSecond_stage_ignition() {
        return second_stage_ignition;
    }

    public void setSecond_stage_ignition(int second_stage_ignition) {
        this.second_stage_ignition = second_stage_ignition;
    }

    public int getCenter_core_boostback() {
        return center_core_boostback;
    }

    public void setCenter_core_boostback(int center_core_boostback) {
        this.center_core_boostback = center_core_boostback;
    }

    public int getFairing_deploy() {
        return fairing_deploy;
    }

    public void setFairing_deploy(int fairing_deploy) {
        this.fairing_deploy = fairing_deploy;
    }

    public int getSide_core_entry_burn() {
        return side_core_entry_burn;
    }

    public void setSide_core_entry_burn(int side_core_entry_burn) {
        this.side_core_entry_burn = side_core_entry_burn;
    }

    public int getCenter_core_entry_burn() {
        return center_core_entry_burn;
    }

    public void setCenter_core_entry_burn(int center_core_entry_burn) {
        this.center_core_entry_burn = center_core_entry_burn;
    }

    public int getSide_core_landing() {
        return side_core_landing;
    }

    public void setSide_core_landing(int side_core_landing) {
        this.side_core_landing = side_core_landing;
    }

    public int getCenter_core_landing() {
        return center_core_landing;
    }

    public void setCenter_core_landing(int center_core_landing) {
        this.center_core_landing = center_core_landing;
    }

    public int getFirst_stage_boostback_burn() {
        return first_stage_boostback_burn;
    }

    public void setFirst_stage_boostback_burn(int first_stage_boostback_burn) {
        this.first_stage_boostback_burn = first_stage_boostback_burn;
    }

    public int getFirst_stage_entry_burn() {
        return first_stage_entry_burn;
    }

    public void setFirst_stage_entry_burn(int first_stage_entry_burn) {
        this.first_stage_entry_burn = first_stage_entry_burn;
    }

    public int getSeco1() {
        return seco1;
    }

    @JsonProperty("seco-1")
    public void setSeco1(int seco1) {
        this.seco1 = seco1;
    }

    public int getSecond_stage_restart() {
        return second_stage_restart;
    }

    public void setSecond_stage_restart(int second_stage_restart) {
        this.second_stage_restart = second_stage_restart;
    }

    @JsonProperty("seco-2")
    public int getSeco2() {
        return seco2;
    }

    public void setSeco2(int seco2) {
        this.seco2 = seco2;
    }

    public int getFirst_stage_landing_burn() {
        return first_stage_landing_burn;
    }

    public void setFirst_stage_landing_burn(int first_stage_landing_burn) {
        this.first_stage_landing_burn = first_stage_landing_burn;
    }

    public int getFirst_stage_landing() {
        return first_stage_landing;
    }

    public void setFirst_stage_landing(int first_stage_landing) {
        this.first_stage_landing = first_stage_landing;
    }

    public int getPayload_deploy() {
        return payload_deploy;
    }

    public void setPayload_deploy(int payload_deploy) {
        this.payload_deploy = payload_deploy;
    }

    public int getPayload_deploy_1() {
        return payload_deploy_1;
    }

    public void setPayload_deploy_1(int payload_deploy_1) {
        this.payload_deploy_1 = payload_deploy_1;
    }

    public int getPayload_deploy_2() {
        return payload_deploy_2;
    }

    public void setPayload_deploy_2(int payload_deploy_2) {
        this.payload_deploy_2 = payload_deploy_2;
    }

    public int getDragon_separation() {
        return dragon_separation;
    }

    public void setDragon_separation(int dragon_separation) {
        this.dragon_separation = dragon_separation;
    }

    public int getDragon_solar_deploy() {
        return dragon_solar_deploy;
    }

    public void setDragon_solar_deploy(int dragon_solar_deploy) {
        this.dragon_solar_deploy = dragon_solar_deploy;
    }

    public int getDragon_bay_door_deploy() {
        return dragon_bay_door_deploy;
    }

    public void setDragon_bay_door_deploy(int dragon_bay_door_deploy) {
        this.dragon_bay_door_deploy = dragon_bay_door_deploy;
    }

}
