package com.paulharding.spacex.gateway.api.model.response.retrieveRockets;

public class PayloadApiResponse {

    private String option_1;
    private String option_2;
    private CompositeFairingApiResponse composite_fairing;

    public String getOption_1() {
        return option_1;
    }

    public void setOption_1(String option_1) {
        this.option_1 = option_1;
    }

    public String getOption_2() {
        return option_2;
    }

    public void setOption_2(String option_2) {
        this.option_2 = option_2;
    }

    public CompositeFairingApiResponse getComposite_fairing() {
        return composite_fairing;
    }

    public void setComposite_fairing(CompositeFairingApiResponse composite_fairing) {
        this.composite_fairing = composite_fairing;
    }

}