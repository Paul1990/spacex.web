package com.paulharding.spacex.gateway.api.deserialize;

public interface Deserializer {

    <T> T deserialize(String json, Class<T> resultClass);

    class FailedDeserializationException extends RuntimeException {

        public FailedDeserializationException() {}

        public FailedDeserializationException(String message) {
            super(message);
        }

    }

}
