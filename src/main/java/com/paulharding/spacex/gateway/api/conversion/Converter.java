package com.paulharding.spacex.gateway.api.conversion;

public interface Converter<Source, Result> {

    Result convert(Source source);

}
