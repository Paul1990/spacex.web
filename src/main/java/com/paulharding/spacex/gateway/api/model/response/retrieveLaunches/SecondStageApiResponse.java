package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class SecondStageApiResponse {

    private int block;
    private PayloadApiResponse[] payloads;

    public int getBlock() {
        return block;
    }

    public void setBlock(int block) {
        this.block = block;
    }

    public PayloadApiResponse[] getPayloads() {
        return payloads;
    }

    public void setPayloads(PayloadApiResponse[] payloads) {
        this.payloads = payloads;
    }

}
