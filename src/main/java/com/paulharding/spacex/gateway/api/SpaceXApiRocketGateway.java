package com.paulharding.spacex.gateway.api;

import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.core.model.rocket.Rocket;
import com.paulharding.spacex.gateway.api.conversion.Converter;
import com.paulharding.spacex.gateway.api.deserialize.Deserializer;
import com.paulharding.spacex.gateway.api.http.HttpRequester;
import com.paulharding.spacex.gateway.api.model.response.retrieveRockets.RocketApiResponse;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpaceXApiRocketGateway implements RocketGateway {

    private static final String RETRIEVE_ROCKET_URL = "https://api.spacexdata.com/v3/rockets";
    private final HttpRequester requester;
    private final Deserializer deserializer;
    private final Converter<RocketApiResponse, Rocket> converter;

    public SpaceXApiRocketGateway(HttpRequester requester, Deserializer deserializer, Converter<RocketApiResponse, Rocket> converter) {
        this.requester = requester;
        this.deserializer = deserializer;
        this.converter = converter;
    }

    @Override
    public List<Rocket> retrieveAll() {

        String json = makeRequest();
        RocketApiResponse[] apiResponse = deserialize(json);

        return convert(apiResponse);

    }

    private String makeRequest() {

        try {
            return requester.makeRequest(RETRIEVE_ROCKET_URL);
        } catch (HttpRequester.FailedHttpRequestException ex) {
            throw new RocketAccessException(ex.getMessage());
        }

    }

    private RocketApiResponse[] deserialize(String json) {

        try {
             return deserializer.deserialize(json, RocketApiResponse[].class);
        } catch (Deserializer.FailedDeserializationException ex) {
            throw new RocketAccessException("Error deserializing retrieve rockets response. Message: " + ex.getMessage());
        }

    }

    private List<Rocket> convert(RocketApiResponse[] apiResponse) {

        return Stream.of(apiResponse)
                .map(converter::convert)
                .collect(Collectors.toList());

    }

}
