package com.paulharding.spacex.gateway.api;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.core.model.launch.Launch;
import com.paulharding.spacex.gateway.api.conversion.Converter;
import com.paulharding.spacex.gateway.api.deserialize.Deserializer;
import com.paulharding.spacex.gateway.api.http.HttpRequester;
import com.paulharding.spacex.gateway.api.model.response.retrieveLaunches.LaunchApiResponse;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpaceXApiLaunchGateway implements LaunchGateway {

    private static final String RETRIEVE_LAUNCH_URL = "https://api.spacexdata.com/v3/launches";
    private final HttpRequester httpRequester;
    private final Deserializer deserializer;
    private final Converter<LaunchApiResponse, Launch> converter;

    public SpaceXApiLaunchGateway(HttpRequester httpRequester, Deserializer deserializer, Converter<LaunchApiResponse, Launch> converter) {

        this.httpRequester = httpRequester;
        this.deserializer = deserializer;
        this.converter = converter;
    }

    @Override
    public List<Launch> retrieveRocketLaunches(String rocketId) {

        String json = makeRequest(rocketId);
        LaunchApiResponse[] apiResponse = deserialize(json);

        return convert(apiResponse);

    }

    private String makeRequest(String rocketId) {

        try {
            String requestUrl = RETRIEVE_LAUNCH_URL + "?rocket_id=" + rocketId;
            return httpRequester.makeRequest(requestUrl);
        } catch (HttpRequester.FailedHttpRequestException ex) {
            throw new LaunchAccessException(ex.getMessage());
        }

    }
    private LaunchApiResponse[] deserialize(String json) {

        try {
            return deserializer.deserialize(json, LaunchApiResponse[].class);
        } catch (Deserializer.FailedDeserializationException ex) {
            throw new LaunchAccessException("Error deserializing retrieve launches response. Message: " + ex.getMessage());
        }

    }
    private List<Launch> convert(LaunchApiResponse[] apiResponse) {

        return Stream.of(apiResponse)
                .map(converter::convert)
                .collect(Collectors.toList());

    }

}
