package com.paulharding.spacex.gateway.api.model.response.retrieveRockets;

public class SecondStageApiResponse {

    private boolean isReusable;
    private int engines;
    private double fuel_amount_tons;
    private double burn_time_sec;
    private ForceMeasurementApiResponse thrust;
    private PayloadApiResponse payloads;

    public boolean isReusable() {
        return isReusable;
    }

    public void setReusable(boolean reusable) {
        isReusable = reusable;
    }

    public int getEngines() {
        return engines;
    }

    public void setEngines(int engines) {
        this.engines = engines;
    }

    public double getFuel_amount_tons() {
        return fuel_amount_tons;
    }

    public void setFuel_amount_tons(double fuel_amount_tons) {
        this.fuel_amount_tons = fuel_amount_tons;
    }

    public double getBurn_time_sec() {
        return burn_time_sec;
    }

    public void setBurn_time_sec(double burn_time_sec) {
        this.burn_time_sec = burn_time_sec;
    }

    public ForceMeasurementApiResponse getThrust() {
        return thrust;
    }

    public void setThrust(ForceMeasurementApiResponse thrust) {
        this.thrust = thrust;
    }

    public PayloadApiResponse getPayloads() {
        return payloads;
    }

    public void setPayloads(PayloadApiResponse payloads) {
        this.payloads = payloads;
    }
}