package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class LaunchApiResponse {

    private long flight_number;
    private String mission_name;
    private String[] mission_id;
    private boolean upcoming;
    private String launch_year;
    private long launch_date_unix;
    private String launch_date_utc;
    private String launch_date_local;
    private boolean is_tentative;
    private String tentative_max_precision;
    private boolean tbd;
    private int launch_window;
    private RocketApiResponse rocket;
    private String[] ships;
    private TelemetryApiResponse telemetry;
    private LaunchSiteApiResponse launch_site;
    private boolean launch_success;
    private LaunchFailureDetailApiResponse launch_failure_details;
    private LaunchLinkApiResponse links;
    private String details;
    private String static_fire_date_utc;
    private String static_fire_date_unix;
    private TimelineApiResponse timeline;

    public long getFlight_number() {
        return flight_number;
    }

    public void setFlight_number(long flight_number) {
        this.flight_number = flight_number;
    }

    public String getMission_name() {
        return mission_name;
    }

    public void setMission_name(String mission_name) {
        this.mission_name = mission_name;
    }

    public String[] getMission_id() {
        return mission_id;
    }

    public void setMission_id(String[] mission_id) {
        this.mission_id = mission_id;
    }

    public boolean isUpcoming() {
        return upcoming;
    }

    public void setUpcoming(boolean upcoming) {
        this.upcoming = upcoming;
    }

    public String getLaunch_year() {
        return launch_year;
    }

    public void setLaunch_year(String launch_year) {
        this.launch_year = launch_year;
    }

    public long getLaunch_date_unix() {
        return launch_date_unix;
    }

    public void setLaunch_date_unix(long launch_date_unix) {
        this.launch_date_unix = launch_date_unix;
    }

    public String getLaunch_date_utc() {
        return launch_date_utc;
    }

    public void setLaunch_date_utc(String launch_date_utc) {
        this.launch_date_utc = launch_date_utc;
    }

    public String getLaunch_date_local() {
        return launch_date_local;
    }

    public void setLaunch_date_local(String launch_date_local) {
        this.launch_date_local = launch_date_local;
    }

    public boolean isIs_tentative() {
        return is_tentative;
    }

    public void setIs_tentative(boolean is_tentative) {
        this.is_tentative = is_tentative;
    }

    public String getTentative_max_precision() {
        return tentative_max_precision;
    }

    public void setTentative_max_precision(String tentative_max_precision) {
        this.tentative_max_precision = tentative_max_precision;
    }

    public boolean isTbd() {
        return tbd;
    }

    public void setTbd(boolean tbd) {
        this.tbd = tbd;
    }

    public int getLaunch_window() {
        return launch_window;
    }

    public void setLaunch_window(int launch_window) {
        this.launch_window = launch_window;
    }

    public RocketApiResponse getRocket() {
        return rocket;
    }

    public void setRocket(RocketApiResponse rocket) {
        this.rocket = rocket;
    }

    public String[] getShips() {
        return ships;
    }

    public void setShips(String[] ships) {
        this.ships = ships;
    }

    public TelemetryApiResponse getTelemetry() {
        return telemetry;
    }

    public void setTelemetry(TelemetryApiResponse telemetry) {
        this.telemetry = telemetry;
    }

    public LaunchSiteApiResponse getLaunch_site() {
        return launch_site;
    }

    public void setLaunch_site(LaunchSiteApiResponse launch_site) {
        this.launch_site = launch_site;
    }

    public boolean isLaunch_success() {
        return launch_success;
    }

    public void setLaunch_success(boolean launch_success) {
        this.launch_success = launch_success;
    }

    public LaunchFailureDetailApiResponse getLaunch_failure_details() {
        return launch_failure_details;
    }

    public void setLaunch_failure_details(LaunchFailureDetailApiResponse launch_failure_details) {
        this.launch_failure_details = launch_failure_details;
    }

    public LaunchLinkApiResponse getLinks() {
        return links;
    }

    public void setLinks(LaunchLinkApiResponse links) {
        this.links = links;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStatic_fire_date_utc() {
        return static_fire_date_utc;
    }

    public void setStatic_fire_date_utc(String static_fire_date_utc) {
        this.static_fire_date_utc = static_fire_date_utc;
    }

    public String getStatic_fire_date_unix() {
        return static_fire_date_unix;
    }

    public void setStatic_fire_date_unix(String static_fire_date_unix) {
        this.static_fire_date_unix = static_fire_date_unix;
    }

    public TimelineApiResponse getTimeline() {
        return timeline;
    }

    public void setTimeline(TimelineApiResponse timeline) {
        this.timeline = timeline;
    }

}
