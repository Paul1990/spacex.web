package com.paulharding.spacex.gateway.api.model.response.retrieveRockets;

public class FirstStageApiResponse {

    private boolean isReusable;
    private int engines;
    private double fuel_amount_tons;
    private int cores;
    private int burn_time_sec;
    private ForceMeasurementApiResponse thrust_sea_level;
    private ForceMeasurementApiResponse thrust_vacuum;

    public boolean isReusable() {
        return isReusable;
    }

    public void setReusable(boolean reusable) {
        isReusable = reusable;
    }

    public int getEngines() {
        return engines;
    }

    public void setEngines(int engines) {
        this.engines = engines;
    }

    public double getFuel_amount_tons() {
        return fuel_amount_tons;
    }

    public void setFuel_amount_tons(double fuel_amount_tons) {
        this.fuel_amount_tons = fuel_amount_tons;
    }

    public int getCores() {
        return cores;
    }

    public void setCores(int cores) {
        this.cores = cores;
    }

    public int getBurn_time_sec() {
        return burn_time_sec;
    }

    public void setBurn_time_sec(int burn_time_sec) {
        this.burn_time_sec = burn_time_sec;
    }

    public ForceMeasurementApiResponse getThrust_sea_level() {
        return thrust_sea_level;
    }

    public void setThrust_sea_level(ForceMeasurementApiResponse thrust_sea_level) {
        this.thrust_sea_level = thrust_sea_level;
    }

    public ForceMeasurementApiResponse getThrust_vacuum() {
        return thrust_vacuum;
    }

    public void setThrust_vacuum(ForceMeasurementApiResponse thrust_vacuum) {
        this.thrust_vacuum = thrust_vacuum;
    }

}