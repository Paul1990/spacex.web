package com.paulharding.spacex.gateway.api.conversion.impl;

import com.paulharding.spacex.core.model.rocket.*;
import com.paulharding.spacex.gateway.api.conversion.Converter;
import com.paulharding.spacex.gateway.api.model.response.retrieveRockets.*;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RocketConverter implements Converter<RocketApiResponse, Rocket> {

    @Override
    public Rocket convert(RocketApiResponse source) {

        return new Rocket(
            source.getId(),
            source.isActive(),
            source.getStages(),
            source.getBoosters(),
            source.getCost_per_launch(),
            source.getSuccess_rate_pct(),
            source.getFirst_flight(),
            source.getCountry(),
            source.getCompany(),
            convert(source.getHeight()),
            convert(source.getDiameter()),
            convert(source.getMass()),
            convert(source.getPayload_weights()),
            convert(source.getFirst_stage()),
            convert(source.getSecond_stage()),
            convert(source.getEngines()),
            convert(source.getLanding_legs()),
            source.getFlickr_images(),
            source.getWikipedia(),
            source.getDescription(),
            source.getRocket_id(),
            source.getRocket_name(),
            source.getRocket_type()
        );

    }

    private Distance convert(DistanceMeasurementApiResponse source) {
        return new Distance(source.getMeters(), source.getFeet());
    }

    private Mass convert(MassMeasurementApiResponse source) {
        return new Mass(source.getKg(), source.getLb());
    }

    private PayloadWeight[] convert(PayloadWeightApiResponse[] source) {

        return Stream.of(source)
                .map(this::convert)
                .collect(Collectors.toList())
                .toArray(new PayloadWeight[source.length]);

    }

    private PayloadWeight convert(PayloadWeightApiResponse source) {

        return new PayloadWeight(
            source.getId(),
            source.getName(),
            new Mass(source.getKg(), source.getLb())
        );

    }

    private FirstStage convert(FirstStageApiResponse source) {

        return new FirstStage(
                source.isReusable(),
                source.getEngines(),
                source.getFuel_amount_tons(),
                source.getCores(),
                source.getBurn_time_sec(),
                convert(source.getThrust_sea_level()),
                convert(source.getThrust_vacuum())
        );

    }

    private Force convert(ForceMeasurementApiResponse source) {
        return new Force(source.getkN(), source.getLbf());
    }

    private SecondStage convert(SecondStageApiResponse source) {

        return new SecondStage(
            source.isReusable(),
            source.getEngines(),
            source.getFuel_amount_tons(),
            source.getBurn_time_sec(),
            convert(source.getThrust()),
            convert(source.getPayloads())
        );

    }

    private Payload convert(PayloadApiResponse source) {

        return new Payload(
            source.getOption_1(),
            source.getOption_2(),
            convert(source.getComposite_fairing())
        );

    }

    private CompositeFairing convert(CompositeFairingApiResponse source) {

        return new CompositeFairing(
            convert(source.getHeight()),
            convert(source.getDiameter())
        );

    }

    private Engine convert(EngineApiResponse source) {

        return new Engine(
                source.getNumber(),
                source.getType(),
                source.getVersion(),
                source.getLayout(),
                source.getEngine_loss_max(),
                source.getPropellant_1(),
                source.getPropellant_2(),
                convert(source.getThrust_sea_level()),
                convert(source.getThrust_vacuum()),
                source.getThrust_to_weight()
        );

    }

    private LandingLeg convert(LandingLegApiResponse source) {
        return new LandingLeg(source.getNumber(), source.getMaterial());
    }

}
