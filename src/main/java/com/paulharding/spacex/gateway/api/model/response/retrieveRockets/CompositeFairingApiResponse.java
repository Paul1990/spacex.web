package com.paulharding.spacex.gateway.api.model.response.retrieveRockets;

public class CompositeFairingApiResponse {

    private DistanceMeasurementApiResponse height;
    private DistanceMeasurementApiResponse diameter;

    public DistanceMeasurementApiResponse getHeight() {
        return height;
    }

    public void setHeight(DistanceMeasurementApiResponse height) {
        this.height = height;
    }

    public DistanceMeasurementApiResponse getDiameter() {
        return diameter;
    }

    public void setDiameter(DistanceMeasurementApiResponse diameter) {
        this.diameter = diameter;
    }

}