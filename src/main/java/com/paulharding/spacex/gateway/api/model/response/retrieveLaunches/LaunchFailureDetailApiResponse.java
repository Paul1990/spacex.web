package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class LaunchFailureDetailApiResponse {

    private int time;
    private int altitude;
    private String reason;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
