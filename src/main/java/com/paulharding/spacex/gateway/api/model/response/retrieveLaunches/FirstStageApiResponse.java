package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class FirstStageApiResponse {

    private CoreApiResponse[] cores;

    public CoreApiResponse[] getCores() {
        return cores;
    }

    public void setCores(CoreApiResponse[] cores) {
        this.cores = cores;
    }

}
