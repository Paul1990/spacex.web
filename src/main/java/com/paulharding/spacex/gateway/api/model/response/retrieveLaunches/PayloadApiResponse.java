package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class PayloadApiResponse {

    private String payload_id;
    private int[] norad_id;
    private String cap_serial;
    private boolean reused;
    private String[] customers;
    private String nationality;
    private String manufacturer;
    private String payload_type;
    private int payload_mass_kg;
    private int payload_mass_lbs;
    private String orbit;
    private OrbitParamApiResponse orbit_params;
    private int mass_returned_kg;
    private int mass_returned_lbs;
    private int flight_time_sec;
    private String cargo_manifest;

    public String getPayload_id() {
        return payload_id;
    }

    public void setPayload_id(String payload_id) {
        this.payload_id = payload_id;
    }

    public int[] getNorad_id() {
        return norad_id;
    }

    public void setNorad_id(int[] norad_id) {
        this.norad_id = norad_id;
    }

    public String getCap_serial() {
        return cap_serial;
    }

    public void setCap_serial(String cap_serial) {
        this.cap_serial = cap_serial;
    }

    public boolean isReused() {
        return reused;
    }

    public void setReused(boolean reused) {
        this.reused = reused;
    }

    public String[] getCustomers() {
        return customers;
    }

    public void setCustomers(String[] customers) {
        this.customers = customers;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPayload_type() {
        return payload_type;
    }

    public void setPayload_type(String payload_type) {
        this.payload_type = payload_type;
    }

    public int getPayload_mass_kg() {
        return payload_mass_kg;
    }

    public void setPayload_mass_kg(int payload_mass_kg) {
        this.payload_mass_kg = payload_mass_kg;
    }

    public int getPayload_mass_lbs() {
        return payload_mass_lbs;
    }

    public void setPayload_mass_lbs(int payload_mass_lbs) {
        this.payload_mass_lbs = payload_mass_lbs;
    }

    public String getOrbit() {
        return orbit;
    }

    public void setOrbit(String orbit) {
        this.orbit = orbit;
    }

    public OrbitParamApiResponse getOrbit_params() {
        return orbit_params;
    }

    public void setOrbit_params(OrbitParamApiResponse orbit_params) {
        this.orbit_params = orbit_params;
    }

    public int getMass_returned_kg() {
        return mass_returned_kg;
    }

    public void setMass_returned_kg(int mass_returned_kg) {
        this.mass_returned_kg = mass_returned_kg;
    }

    public int getMass_returned_lbs() {
        return mass_returned_lbs;
    }

    public void setMass_returned_lbs(int mass_returned_lbs) {
        this.mass_returned_lbs = mass_returned_lbs;
    }

    public int getFlight_time_sec() {
        return flight_time_sec;
    }

    public void setFlight_time_sec(int flight_time_sec) {
        this.flight_time_sec = flight_time_sec;
    }

    public String getCargo_manifest() {
        return cargo_manifest;
    }

    public void setCargo_manifest(String cargo_manifest) {
        this.cargo_manifest = cargo_manifest;
    }
}
