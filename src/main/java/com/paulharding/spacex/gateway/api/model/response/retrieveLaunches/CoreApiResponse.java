package com.paulharding.spacex.gateway.api.model.response.retrieveLaunches;

public class CoreApiResponse {

    private String core_serial;
    private String flight;
    private int block;
    private boolean gridfins;
    private boolean legs;
    private boolean reused;
    private boolean land_success;
    private boolean landing_intent;
    private String landing_type;
    private String landing_vehicle;

    public String getCore_serial() {
        return core_serial;
    }

    public void setCore_serial(String core_serial) {
        this.core_serial = core_serial;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public int getBlock() {
        return block;
    }

    public void setBlock(int block) {
        this.block = block;
    }

    public boolean isGridfins() {
        return gridfins;
    }

    public void setGridfins(boolean gridfins) {
        this.gridfins = gridfins;
    }

    public boolean isLegs() {
        return legs;
    }

    public void setLegs(boolean legs) {
        this.legs = legs;
    }

    public boolean isReused() {
        return reused;
    }

    public void setReused(boolean reused) {
        this.reused = reused;
    }

    public boolean isLand_success() {
        return land_success;
    }

    public void setLand_success(boolean land_success) {
        this.land_success = land_success;
    }

    public boolean isLanding_intent() {
        return landing_intent;
    }

    public void setLanding_intent(boolean landing_intent) {
        this.landing_intent = landing_intent;
    }

    public String getLanding_type() {
        return landing_type;
    }

    public void setLanding_type(String landing_type) {
        this.landing_type = landing_type;
    }

    public String getLanding_vehicle() {
        return landing_vehicle;
    }

    public void setLanding_vehicle(String landing_vehicle) {
        this.landing_vehicle = landing_vehicle;
    }

}
