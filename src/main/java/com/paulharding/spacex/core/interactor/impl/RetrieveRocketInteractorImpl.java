package com.paulharding.spacex.core.interactor.impl;

import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.core.interactor.RetrieveRocketInteractor;
import com.paulharding.spacex.core.model.rocket.Rocket;

import java.util.List;

public class RetrieveRocketInteractorImpl implements RetrieveRocketInteractor {

    private final RocketGateway gateway;

    public RetrieveRocketInteractorImpl(RocketGateway gateway) {
        this.gateway = gateway;
    }

    @Override
    public List<Rocket> retrieveAll() {
        return gateway.retrieveAll();
    }

}
