package com.paulharding.spacex.core.interactor;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.core.model.launch.Launch;

import java.util.List;

public interface RetrieveLaunchInteractor {

    /**
     * Retrieve a List of launches associated with the given rocket
     *
     * @param rocketId the ID of the rocket to retrieve launches for
     * @return a List of the given rocket's launches
     * @throws LaunchGateway.LaunchAccessException if there is an error retrieving the launches
     */
    List<Launch> retrieveRocketLaunches(String rocketId);

}
