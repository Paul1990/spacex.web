package com.paulharding.spacex.core.interactor.impl;

import com.paulharding.spacex.core.gateway.LaunchGateway;
import com.paulharding.spacex.core.interactor.RetrieveLaunchInteractor;
import com.paulharding.spacex.core.model.launch.Launch;

import java.util.List;

public class RetrieveLaunchInteractorImpl implements RetrieveLaunchInteractor {

    private final LaunchGateway launchGateway;

    public RetrieveLaunchInteractorImpl(LaunchGateway launchGateway) {
        this.launchGateway = launchGateway;
    }

    @Override
    public List<Launch> retrieveRocketLaunches(String rocketId) {
        return launchGateway.retrieveRocketLaunches(rocketId);
    }

}
