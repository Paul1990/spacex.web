package com.paulharding.spacex.core.interactor;

import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.core.model.rocket.Rocket;

import java.util.List;

public interface RetrieveRocketInteractor {

    /**
     * Retrieves all of the rockets
     *
     * @return a List of all of the rockets
     * @throws RocketGateway.RocketAccessException if there is an error accessing the rocket data
     */
    List<Rocket> retrieveAll();

}
