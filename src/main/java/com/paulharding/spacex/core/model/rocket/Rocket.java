package com.paulharding.spacex.core.model.rocket;

import java.util.Calendar;

public class Rocket {

    private final long id;
    private final boolean isActive;
    private final int numberOfStages;
    private final int numberOfBoosters;
    private final int costPerLaunch;
    private final double successRatePercentage;
    private final Calendar firstFlight;
    private final String country;
    private final String company;
    private final Distance height;
    private final Distance diameter;
    private final Mass mass;
    private final PayloadWeight[] payloadWeight;
    private final FirstStage firstStage;
    private final SecondStage secondStage;
    private final Engine engine;
    private final LandingLeg landingLeg;
    private final String[] imageUrls;
    private final String wikipediaUrl;
    private final String description;
    private final String rocketId;
    private final String rocketName;
    private final String rocketType;

    public Rocket(long id, boolean isActive, int numberOfStages, int numberOfBoosters, int costPerLaunch, double successRatePercentage, Calendar firstFlight, String country, String company, Distance height, Distance diameter, Mass mass, PayloadWeight[] payloadWeight, FirstStage firstStage, SecondStage secondStage, Engine engine, LandingLeg landingLeg, String[] imageUrls, String wikipediaUrl, String description, String rocketId, String rocketName, String rocketType) {
        this.id = id;
        this.isActive = isActive;
        this.numberOfStages = numberOfStages;
        this.numberOfBoosters = numberOfBoosters;
        this.costPerLaunch = costPerLaunch;
        this.successRatePercentage = successRatePercentage;
        this.firstFlight = firstFlight;
        this.country = country;
        this.company = company;
        this.height = height;
        this.diameter = diameter;
        this.mass = mass;
        this.payloadWeight = payloadWeight;
        this.firstStage = firstStage;
        this.secondStage = secondStage;
        this.engine = engine;
        this.landingLeg = landingLeg;
        this.imageUrls = imageUrls;
        this.wikipediaUrl = wikipediaUrl;
        this.description = description;
        this.rocketId = rocketId;
        this.rocketName = rocketName;
        this.rocketType = rocketType;
    }

    public long getId() {
        return id;
    }

    public boolean isActive() {
        return isActive;
    }

    public int getNumberOfStages() {
        return numberOfStages;
    }

    public int getNumberOfBoosters() {
        return numberOfBoosters;
    }

    public int getCostPerLaunch() {
        return costPerLaunch;
    }

    public double getSuccessRatePercentage() {
        return successRatePercentage;
    }

    public Calendar getFirstFlight() {
        return firstFlight;
    }

    public String getCountry() {
        return country;
    }

    public String getCompany() {
        return company;
    }

    public Distance getHeight() {
        return height;
    }

    public Distance getDiameter() {
        return diameter;
    }

    public Mass getMass() {
        return mass;
    }

    public PayloadWeight[] getPayloadWeight() {
        return payloadWeight;
    }

    public FirstStage getFirstStage() {
        return firstStage;
    }

    public SecondStage getSecondStage() {
        return secondStage;
    }

    public Engine getEngine() {
        return engine;
    }

    public LandingLeg getLandingLeg() {
        return landingLeg;
    }

    public String[] getImageUrls() {
        return imageUrls;
    }

    public String getWikipediaUrl() {
        return wikipediaUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getRocketId() {
        return rocketId;
    }

    public String getRocketName() {
        return rocketName;
    }

    public String getRocketType() {
        return rocketType;
    }

}
