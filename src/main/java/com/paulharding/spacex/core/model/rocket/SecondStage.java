package com.paulharding.spacex.core.model.rocket;

public class SecondStage {

    private final boolean isReusable;
    private final int numberOfEngines;
    private final double tonsOfFuel;
    private final double secondsOfBurnTime;
    private final Force thrust;
    private final Payload payload;

    public SecondStage(boolean isReusable, int numberOfEngines, double tonsOfFuel, double secondsOfBurnTime, Force thrust, Payload payload) {
        this.isReusable = isReusable;
        this.numberOfEngines = numberOfEngines;
        this.tonsOfFuel = tonsOfFuel;
        this.secondsOfBurnTime = secondsOfBurnTime;
        this.thrust = thrust;
        this.payload = payload;
    }

    public boolean isReusable() {
        return isReusable;
    }

    public int getNumberOfEngines() {
        return numberOfEngines;
    }

    public double getTonsOfFuel() {
        return tonsOfFuel;
    }

    public double getSecondsOfBurnTime() {
        return secondsOfBurnTime;
    }

    public Force getThrust() {
        return thrust;
    }

    public Payload getPayload() {
        return payload;
    }

}
