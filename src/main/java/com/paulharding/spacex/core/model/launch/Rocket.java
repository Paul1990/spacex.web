package com.paulharding.spacex.core.model.launch;

public class Rocket {

    private final String id;
    private final String name;
    private final String type;
    private final FirstStage firstStage;
    private final SecondStage secondStage;
    private final Fairing fairings;

    public Rocket(String id, String name, String type, FirstStage firstStage, SecondStage secondStage, Fairing fairings) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.firstStage = firstStage;
        this.secondStage = secondStage;
        this.fairings = fairings;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public FirstStage getFirstStage() {
        return firstStage;
    }

    public SecondStage getSecondStage() {
        return secondStage;
    }

    public Fairing getFairings() {
        return fairings;
    }

}
