package com.paulharding.spacex.core.model.launch;

public class Orbit {

    private final String referenceSystem;
    private final String regime;
    private final double longitude;
    private final double semiMajorAxisKilometers;
    private final double eccentricity;
    private final int periapsisKilometers;
    private final int apoapsisKilometers;
    private final int inclinationDegrees;
    private final double periodMinimum;
    private final int lifeSpanInYears;
    private final String epoch;
    private final double meanMotion;
    private final double raan;
    private final double argOfPericenter;
    private final double meanAnomaly;

    public Orbit(String referenceSystem, String regime, double longitude, double semiMajorAxisKilometers, double eccentricity, int periapsisKilometers, int apoapsisKilometers, int inclinationDegrees, double periodMinimum, int lifeSpanInYears, String epoch, double meanMotion, double raan, double argOfPericenter, double meanAnomaly) {
        this.referenceSystem = referenceSystem;
        this.regime = regime;
        this.longitude = longitude;
        this.semiMajorAxisKilometers = semiMajorAxisKilometers;
        this.eccentricity = eccentricity;
        this.periapsisKilometers = periapsisKilometers;
        this.apoapsisKilometers = apoapsisKilometers;
        this.inclinationDegrees = inclinationDegrees;
        this.periodMinimum = periodMinimum;
        this.lifeSpanInYears = lifeSpanInYears;
        this.epoch = epoch;
        this.meanMotion = meanMotion;
        this.raan = raan;
        this.argOfPericenter = argOfPericenter;
        this.meanAnomaly = meanAnomaly;
    }

    public String getReferenceSystem() {
        return referenceSystem;
    }

    public String getRegime() {
        return regime;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getSemiMajorAxisKilometers() {
        return semiMajorAxisKilometers;
    }

    public double getEccentricity() {
        return eccentricity;
    }

    public int getPeriapsisKilometers() {
        return periapsisKilometers;
    }

    public int getApoapsisKilometers() {
        return apoapsisKilometers;
    }

    public int getInclinationDegrees() {
        return inclinationDegrees;
    }

    public double getPeriodMinimum() {
        return periodMinimum;
    }

    public int getLifeSpanInYears() {
        return lifeSpanInYears;
    }

    public String getEpoch() {
        return epoch;
    }

    public double getMeanMotion() {
        return meanMotion;
    }

    public double getRaan() {
        return raan;
    }

    public double getArgOfPericenter() {
        return argOfPericenter;
    }

    public double getMeanAnomaly() {
        return meanAnomaly;
    }
}
