package com.paulharding.spacex.core.model.launch;

public class Core {

    private final String coreSerial;
    private final String flight;
    private final int block;
    private final boolean gridfins;
    private final boolean legs;
    private final boolean isReused;
    private final boolean isLandSuccessful;
    private final boolean isLandingIntent;
    private final String landingType;
    private final String landingVehicle;

    public Core(String coreSerial, String flight, int block, boolean gridfins, boolean legs, boolean isReused, boolean isLandSuccessful, boolean isLandingIntent, String landingType, String landingVehicle) {
        this.coreSerial = coreSerial;
        this.flight = flight;
        this.block = block;
        this.gridfins = gridfins;
        this.legs = legs;
        this.isReused = isReused;
        this.isLandSuccessful = isLandSuccessful;
        this.isLandingIntent = isLandingIntent;
        this.landingType = landingType;
        this.landingVehicle = landingVehicle;
    }

    public String getCoreSerial() {
        return coreSerial;
    }

    public String getFlight() {
        return flight;
    }

    public int getBlock() {
        return block;
    }

    public boolean isGridfins() {
        return gridfins;
    }

    public boolean isLegs() {
        return legs;
    }

    public boolean isReused() {
        return isReused;
    }

    public boolean isLandSuccessful() {
        return isLandSuccessful;
    }

    public boolean isLandingIntent() {
        return isLandingIntent;
    }

    public String getLandingType() {
        return landingType;
    }

    public String getLandingVehicle() {
        return landingVehicle;
    }

}
