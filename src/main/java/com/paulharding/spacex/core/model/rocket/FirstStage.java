package com.paulharding.spacex.core.model.rocket;

public class FirstStage {

    private final boolean isReusable;
    private final int numberOfEngines;
    private final double tonsOfFuel;
    private final int numberOfCores;
    private final int secondsOfBurnTime;
    private final Force seaLevelThrust;
    private final Force vacuumThrust;

    public FirstStage(boolean isReusable, int numberOfEngines, double tonsOfFuel, int numberOfCores, int secondsOfBurnTime, Force seaLevelThrust, Force vacuumThrust) {
        this.isReusable = isReusable;
        this.numberOfEngines = numberOfEngines;
        this.tonsOfFuel = tonsOfFuel;
        this.numberOfCores = numberOfCores;
        this.secondsOfBurnTime = secondsOfBurnTime;
        this.seaLevelThrust = seaLevelThrust;
        this.vacuumThrust = vacuumThrust;
    }

    public boolean isReusable() {
        return isReusable;
    }

    public int getNumberOfEngines() {
        return numberOfEngines;
    }

    public double getTonsOfFuel() {
        return tonsOfFuel;
    }

    public int getNumberOfCores() {
        return numberOfCores;
    }

    public int getSecondsOfBurnTime() {
        return secondsOfBurnTime;
    }

    public Force getSeaLevelThrust() {
        return seaLevelThrust;
    }

    public Force getVacuumThrust() {
        return vacuumThrust;
    }

}
