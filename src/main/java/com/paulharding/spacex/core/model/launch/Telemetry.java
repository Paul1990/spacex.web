package com.paulharding.spacex.core.model.launch;

public class Telemetry {

    private final String flightClub;

    public Telemetry(String flightClub) {
        this.flightClub = flightClub;
    }

    public String getFlightClub() {
        return flightClub;
    }

}
