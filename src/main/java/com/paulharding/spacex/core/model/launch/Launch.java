package com.paulharding.spacex.core.model.launch;

public class Launch {

    private final long flightNumber;
    private final String missionName;
    private final String[] missionIds;
    private final boolean isUpcoming;
    private final String launchYear;
    private final long unixLaunchDate;
    private final String utcLaunchDate;
    private final String localLaunchDate;
    private final boolean isTentative;
    private final String tentativeMaxPrecision;
    private final boolean isTbd;
    private final int launchWindow;
    private final Rocket rocket;
    private final String[] ships;
    private final Telemetry telemetry;
    private final LaunchSite launchSite;
    private final boolean isLaunchSuccessful;
    private final LaunchFailureDetail launchFailureDetail;
    private final LaunchLink links;
    private final String details;
    private final String utcStaticFireDate;
    private final String unixStaticFireDate;
    private final Timeline timeline;

    public Launch(long flightNumber, String missionName, String[] missionIds, boolean isUpcoming, String launchYear, long unixLaunchDate, String utcLaunchDate, String localLaunchDate, boolean isTentative, String tentativeMaxPrecision, boolean isTbd, int launchWindow, Rocket rocket, String[] ships, Telemetry telemetry, LaunchSite launchSite, boolean isLaunchSuccessful, LaunchFailureDetail launchFailureDetail, LaunchLink links, String details, String utcStaticFireDate, String unixStaticFireDate, Timeline timeline) {
        this.flightNumber = flightNumber;
        this.missionName = missionName;
        this.missionIds = missionIds;
        this.isUpcoming = isUpcoming;
        this.launchYear = launchYear;
        this.unixLaunchDate = unixLaunchDate;
        this.utcLaunchDate = utcLaunchDate;
        this.localLaunchDate = localLaunchDate;
        this.isTentative = isTentative;
        this.tentativeMaxPrecision = tentativeMaxPrecision;
        this.isTbd = isTbd;
        this.launchWindow = launchWindow;
        this.rocket = rocket;
        this.ships = ships;
        this.telemetry = telemetry;
        this.launchSite = launchSite;
        this.isLaunchSuccessful = isLaunchSuccessful;
        this.launchFailureDetail = launchFailureDetail;
        this.links = links;
        this.details = details;
        this.utcStaticFireDate = utcStaticFireDate;
        this.unixStaticFireDate = unixStaticFireDate;
        this.timeline = timeline;
    }

    public long getFlightNumber() {
        return flightNumber;
    }

    public String getMissionName() {
        return missionName;
    }

    public String[] getMissionIds() {
        return missionIds;
    }

    public boolean isUpcoming() {
        return isUpcoming;
    }

    public String getLaunchYear() {
        return launchYear;
    }

    public long getUnixLaunchDate() {
        return unixLaunchDate;
    }

    public String getUtcLaunchDate() {
        return utcLaunchDate;
    }

    public String getLocalLaunchDate() {
        return localLaunchDate;
    }

    public boolean isTentative() {
        return isTentative;
    }

    public String getTentativeMaxPrecision() {
        return tentativeMaxPrecision;
    }

    public boolean isTbd() {
        return isTbd;
    }

    public int getLaunchWindow() {
        return launchWindow;
    }

    public Rocket getRocket() {
        return rocket;
    }

    public String[] getShips() {
        return ships;
    }

    public Telemetry getTelemetry() {
        return telemetry;
    }

    public LaunchSite getLaunchSite() {
        return launchSite;
    }

    public boolean isLaunchSuccessful() {
        return isLaunchSuccessful;
    }

    public LaunchFailureDetail getLaunchFailureDetail() {
        return launchFailureDetail;
    }

    public LaunchLink getLinks() {
        return links;
    }

    public String getDetails() {
        return details;
    }

    public String getUtcStaticFireDate() {
        return utcStaticFireDate;
    }

    public String getUnixStaticFireDate() {
        return unixStaticFireDate;
    }

    public Timeline getTimeline() {
        return timeline;
    }

}
