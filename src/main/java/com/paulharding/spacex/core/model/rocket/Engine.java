package com.paulharding.spacex.core.model.rocket;

public class Engine {

    private final int number;
    private final String type;
    private final String version;
    private final String layout;
    private final int maxEngineLoss;
    private final String propellant1;
    private final String propellant2;
    private final Force seaLevelThrust;
    private final Force vacuumThrust;
    private final int thrustToWeight;

    public Engine(int number, String type, String version, String layout, int maxEngineLoss, String propellant1, String propellant2, Force seaLevelThrust, Force vacuumThrust, int thrustToWeight) {
        this.number = number;
        this.type = type;
        this.version = version;
        this.layout = layout;
        this.maxEngineLoss = maxEngineLoss;
        this.propellant1 = propellant1;
        this.propellant2 = propellant2;
        this.seaLevelThrust = seaLevelThrust;
        this.vacuumThrust = vacuumThrust;
        this.thrustToWeight = thrustToWeight;
    }

    public int getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }

    public String getVersion() {
        return version;
    }

    public String getLayout() {
        return layout;
    }

    public int getMaxEngineLoss() {
        return maxEngineLoss;
    }

    public String getPropellant1() {
        return propellant1;
    }

    public String getPropellant2() {
        return propellant2;
    }

    public Force getSeaLevelThrust() {
        return seaLevelThrust;
    }

    public Force getVacuumThrust() {
        return vacuumThrust;
    }

    public int getThrustToWeight() {
        return thrustToWeight;
    }

}
