package com.paulharding.spacex.core.model.launch;

public class LaunchLink {

    private final String missionPatch;
    private final String missionPatchSmall;
    private final String redditCampaign;
    private final String redditLaunch;
    private final String redditRecovery;
    private final String redditMedia;
    private final String pressKit;
    private final String articleLink;
    private final String wikipedia;
    private final String videoLink;
    private final String youtubeId;
    private final String[] flickrImages;

    public LaunchLink(String missionPatch, String missionPatchSmall, String redditCampaign, String redditLaunch, String redditRecovery, String redditMedia, String pressKit, String articleLink, String wikipedia, String videoLink, String youtubeId, String[] flickrImages) {
        this.missionPatch = missionPatch;
        this.missionPatchSmall = missionPatchSmall;
        this.redditCampaign = redditCampaign;
        this.redditLaunch = redditLaunch;
        this.redditRecovery = redditRecovery;
        this.redditMedia = redditMedia;
        this.pressKit = pressKit;
        this.articleLink = articleLink;
        this.wikipedia = wikipedia;
        this.videoLink = videoLink;
        this.youtubeId = youtubeId;
        this.flickrImages = flickrImages;
    }

    public String getMissionPatch() {
        return missionPatch;
    }

    public String getMissionPatchSmall() {
        return missionPatchSmall;
    }

    public String getRedditCampaign() {
        return redditCampaign;
    }

    public String getRedditLaunch() {
        return redditLaunch;
    }

    public String getRedditRecovery() {
        return redditRecovery;
    }

    public String getRedditMedia() {
        return redditMedia;
    }

    public String getPressKit() {
        return pressKit;
    }

    public String getArticleLink() {
        return articleLink;
    }

    public String getWikipedia() {
        return wikipedia;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public String[] getFlickrImages() {
        return flickrImages;
    }

}
