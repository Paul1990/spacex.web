package com.paulharding.spacex.core.model.rocket;

public class CompositeFairing {

    private final Distance height;
    private final Distance diameter;

    public CompositeFairing(Distance height, Distance diameter) {
        this.height = height;
        this.diameter = diameter;
    }

    public Distance getHeight() {
        return height;
    }

    public Distance getDiameter() {
        return diameter;
    }

}
