package com.paulharding.spacex.core.model.launch;

public class SecondStage {

    private final int block;
    private final Payload[] payloads;

    public SecondStage(int block, Payload[] payloads) {
        this.block = block;
        this.payloads = payloads;
    }

    public int getBlock() {
        return block;
    }

    public Payload[] getPayloads() {
        return payloads;
    }

}
