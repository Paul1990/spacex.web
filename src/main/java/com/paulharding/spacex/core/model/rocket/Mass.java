package com.paulharding.spacex.core.model.rocket;

public class Mass {

    private final int kilograms;
    private final int pounds;

    public Mass(int kilograms, int pounds) {
        this.kilograms = kilograms;
        this.pounds = pounds;
    }

    public int getKilograms() {
        return kilograms;
    }

    public int getPounds() {
        return pounds;
    }

}
