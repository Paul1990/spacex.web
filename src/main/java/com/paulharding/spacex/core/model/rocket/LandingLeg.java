package com.paulharding.spacex.core.model.rocket;

public class LandingLeg {

    private final int number;
    private final String material;

    public LandingLeg(int number, String material) {
        this.number = number;
        this.material = material;
    }

    public int getNumber() {
        return number;
    }

    public String getMaterial() {
        return material;
    }

}
