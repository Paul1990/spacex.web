package com.paulharding.spacex.core.model.launch;

public class LaunchSite {

    private final String id;
    private final String name;
    private final String longName;

    public LaunchSite(String id, String name, String longName) {
        this.id = id;
        this.name = name;
        this.longName = longName;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLongName() {
        return longName;
    }

}
