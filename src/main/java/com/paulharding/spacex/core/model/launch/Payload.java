package com.paulharding.spacex.core.model.launch;

public class Payload {

    private final String id;
    private final int[] noradId;
    private final String capSerial;
    private final boolean isReused;
    private final String[] customers;
    private final String nationality;
    private final String manufacturer;
    private final String type;
    private final int massInKilograms;
    private final int massInPounds;
    private final String orbit;
    private final Orbit orbitParam;
    private final int mass_returned_kg;
    private final int mass_returned_lbs;
    private final int flight_time_sec;
    private final String cargo_manifest;

    public Payload(String id, int[] noradId, String capSerial, boolean isReused, String[] customers, String nationality, String manufacturer, String type, int massInKilograms, int massInPounds, String orbit, Orbit orbitParam, int mass_returned_kg, int mass_returned_lbs, int flight_time_sec, String cargo_manifest) {
        this.id = id;
        this.noradId = noradId;
        this.capSerial = capSerial;
        this.isReused = isReused;
        this.customers = customers;
        this.nationality = nationality;
        this.manufacturer = manufacturer;
        this.type = type;
        this.massInKilograms = massInKilograms;
        this.massInPounds = massInPounds;
        this.orbit = orbit;
        this.orbitParam = orbitParam;
        this.mass_returned_kg = mass_returned_kg;
        this.mass_returned_lbs = mass_returned_lbs;
        this.flight_time_sec = flight_time_sec;
        this.cargo_manifest = cargo_manifest;
    }

    public String getId() {
        return id;
    }

    public int[] getNoradId() {
        return noradId;
    }

    public String getCapSerial() {
        return capSerial;
    }

    public boolean isReused() {
        return isReused;
    }

    public String[] getCustomers() {
        return customers;
    }

    public String getNationality() {
        return nationality;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getType() {
        return type;
    }

    public int getMassInKilograms() {
        return massInKilograms;
    }

    public int getMassInPounds() {
        return massInPounds;
    }

    public String getOrbit() {
        return orbit;
    }

    public Orbit getOrbitParam() {
        return orbitParam;
    }

    public int getMass_returned_kg() {
        return mass_returned_kg;
    }

    public int getMass_returned_lbs() {
        return mass_returned_lbs;
    }

    public int getFlight_time_sec() {
        return flight_time_sec;
    }

    public String getCargo_manifest() {
        return cargo_manifest;
    }

}
