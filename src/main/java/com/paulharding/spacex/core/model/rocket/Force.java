package com.paulharding.spacex.core.model.rocket;

public class Force {

    private final int kiloNewtons;
    private final int poundForce;

    public Force(int kiloNewtons, int poundForce) {
        this.kiloNewtons = kiloNewtons;
        this.poundForce = poundForce;
    }

    public int getKiloNewtons() {
        return kiloNewtons;
    }

    public int getPoundForce() {
        return poundForce;
    }

}
