package com.paulharding.spacex.core.model.launch;

public class LaunchFailureDetail {

    private final int time;
    private final int altitude;
    private final String reason;

    public LaunchFailureDetail(int time, int altitude, String reason) {
        this.time = time;
        this.altitude = altitude;
        this.reason = reason;
    }

    public int getTime() {
        return time;
    }

    public int getAltitude() {
        return altitude;
    }

    public String getReason() {
        return reason;
    }

}
