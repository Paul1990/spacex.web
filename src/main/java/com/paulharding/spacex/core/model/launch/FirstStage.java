package com.paulharding.spacex.core.model.launch;

public class FirstStage {

    private final Core[] cores;

    public FirstStage(Core[] cores) {
        this.cores = cores;
    }

    public Core[] getCores() {
        return cores;
    }

}
