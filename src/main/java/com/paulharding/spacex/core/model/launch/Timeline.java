package com.paulharding.spacex.core.model.launch;

public class Timeline {

    private final int webcastLiftoff;
    private final int webcastLaunch;
    private final int goForPropLaoding;
    private final int rp1Loading;
    private final int stage1LoxLoading;
    private final int stage2LoxLoading;
    private final int engineChill;
    private final int prelaunchChecks;
    private final int propellantPressurization;
    private final int goForLaunch;
    private final int ignition;
    private final int liftoff;
    private final int maxq;
    private final int beco;
    private final int sideCoreSep;
    private final int sideCoreBoostback;
    private final int meco;
    private final int stageStep;
    private final int centerStageSep;
    private final int secondStageIgnition;
    private final int centerCoreBoostback;
    private final int fairingDeploy;
    private final int sideCoreEntryBurn;
    private final int centerCoreEntryBurn;
    private final int sideCoreLanding;
    private final int centerCoreLanding;
    private final int firstStageBoostbackBurn;
    private final int firstStageEntryBurn;
    private final int seco1;
    private final int secondStageRestart;
    private final int seco2;
    private final int firstStageLandingBurn;
    private final int firstStageLanding;
    private final int payloadDeploy;
    private final int payloadDeploy1;
    private final int payloadDeploy2;
    private final int dragonSeparation;
    private final int dragonSolarDeploy;
    private final int dragonBayDoorDeploy;

    public Timeline(int webcastLiftoff, int webcastLaunch, int goForPropLaoding, int rp1Loading, int stage1LoxLoading, int stage2LoxLoading, int engineChill, int prelaunchChecks, int propellantPressurization, int goForLaunch, int ignition, int liftoff, int maxq, int beco, int sideCoreSep, int sideCoreBoostback, int meco, int stageStep, int centerStageSep, int secondStageIgnition, int centerCoreBoostback, int fairingDeploy, int sideCoreEntryBurn, int centerCoreEntryBurn, int sideCoreLanding, int centerCoreLanding, int firstStageBoostbackBurn, int firstStageEntryBurn, int seco1, int secondStageRestart, int seco2, int firstStageLandingBurn, int firstStageLanding, int payloadDeploy, int payloadDeploy1, int payloadDeploy2, int dragonSeparation, int dragonSolarDeploy, int dragonBayDoorDeploy) {
        this.webcastLiftoff = webcastLiftoff;
        this.webcastLaunch = webcastLaunch;
        this.goForPropLaoding = goForPropLaoding;
        this.rp1Loading = rp1Loading;
        this.stage1LoxLoading = stage1LoxLoading;
        this.stage2LoxLoading = stage2LoxLoading;
        this.engineChill = engineChill;
        this.prelaunchChecks = prelaunchChecks;
        this.propellantPressurization = propellantPressurization;
        this.goForLaunch = goForLaunch;
        this.ignition = ignition;
        this.liftoff = liftoff;
        this.maxq = maxq;
        this.beco = beco;
        this.sideCoreSep = sideCoreSep;
        this.sideCoreBoostback = sideCoreBoostback;
        this.meco = meco;
        this.stageStep = stageStep;
        this.centerStageSep = centerStageSep;
        this.secondStageIgnition = secondStageIgnition;
        this.centerCoreBoostback = centerCoreBoostback;
        this.fairingDeploy = fairingDeploy;
        this.sideCoreEntryBurn = sideCoreEntryBurn;
        this.centerCoreEntryBurn = centerCoreEntryBurn;
        this.sideCoreLanding = sideCoreLanding;
        this.centerCoreLanding = centerCoreLanding;
        this.firstStageBoostbackBurn = firstStageBoostbackBurn;
        this.firstStageEntryBurn = firstStageEntryBurn;
        this.seco1 = seco1;
        this.secondStageRestart = secondStageRestart;
        this.seco2 = seco2;
        this.firstStageLandingBurn = firstStageLandingBurn;
        this.firstStageLanding = firstStageLanding;
        this.payloadDeploy = payloadDeploy;
        this.payloadDeploy1 = payloadDeploy1;
        this.payloadDeploy2 = payloadDeploy2;
        this.dragonSeparation = dragonSeparation;
        this.dragonSolarDeploy = dragonSolarDeploy;
        this.dragonBayDoorDeploy = dragonBayDoorDeploy;
    }

    public int getWebcastLiftoff() {
        return webcastLiftoff;
    }

    public int getWebcastLaunch() {
        return webcastLaunch;
    }

    public int getGoForPropLaoding() {
        return goForPropLaoding;
    }

    public int getRp1Loading() {
        return rp1Loading;
    }

    public int getStage1LoxLoading() {
        return stage1LoxLoading;
    }

    public int getStage2LoxLoading() {
        return stage2LoxLoading;
    }

    public int getEngineChill() {
        return engineChill;
    }

    public int getPrelaunchChecks() {
        return prelaunchChecks;
    }

    public int getPropellantPressurization() {
        return propellantPressurization;
    }

    public int getGoForLaunch() {
        return goForLaunch;
    }

    public int getIgnition() {
        return ignition;
    }

    public int getLiftoff() {
        return liftoff;
    }

    public int getMaxq() {
        return maxq;
    }

    public int getBeco() {
        return beco;
    }

    public int getSideCoreSep() {
        return sideCoreSep;
    }

    public int getSideCoreBoostback() {
        return sideCoreBoostback;
    }

    public int getMeco() {
        return meco;
    }

    public int getStageStep() {
        return stageStep;
    }

    public int getCenterStageSep() {
        return centerStageSep;
    }

    public int getSecondStageIgnition() {
        return secondStageIgnition;
    }

    public int getCenterCoreBoostback() {
        return centerCoreBoostback;
    }

    public int getFirstStageBoostbackBurn() {
        return firstStageBoostbackBurn;
    }

    public int getFirstStageEntryBurn() {
        return firstStageEntryBurn;
    }

    public int getSeco1() {
        return seco1;
    }

    public int getSecondStageRestart() {
        return secondStageRestart;
    }

    public int getSeco2() {
        return seco2;
    }

    public int getFirstStageLandingBurn() {
        return firstStageLandingBurn;
    }

    public int getFirstStageLanding() {
        return firstStageLanding;
    }

    public int getPayloadDeploy() {
        return payloadDeploy;
    }

    public int getPayloadDeploy1() {
        return payloadDeploy1;
    }

    public int getPayloadDeploy2() {
        return payloadDeploy2;
    }

    public int getFairingDeploy() {
        return fairingDeploy;
    }

    public int getSideCoreEntryBurn() {
        return sideCoreEntryBurn;
    }

    public int getCenterCoreEntryBurn() {
        return centerCoreEntryBurn;
    }

    public int getSideCoreLanding() {
        return sideCoreLanding;
    }

    public int getCenterCoreLanding() {
        return centerCoreLanding;
    }

    public int getDragonSeparation() {
        return dragonSeparation;
    }

    public int getDragonSolarDeploy() {
        return dragonSolarDeploy;
    }

    public int getDragonBayDoorDeploy() {
        return dragonBayDoorDeploy;
    }

}
