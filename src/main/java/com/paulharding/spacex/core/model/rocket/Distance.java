package com.paulharding.spacex.core.model.rocket;

public class Distance {

    private final double meters;
    private final double feet;

    public Distance(double meters, double feet) {
        this.meters = meters;
        this.feet = feet;
    }

    public double getMeters() {
        return meters;
    }

    public double getFeet() {
        return feet;
    }

}
