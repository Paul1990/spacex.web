package com.paulharding.spacex.core.model.rocket;

public class PayloadWeight {

    private final String id;
    private final String name;
    private final Mass mass;

    public PayloadWeight(String id, String name, Mass mass) {
        this.id = id;
        this.name = name;
        this.mass = mass;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Mass getMass() {
        return mass;
    }

}
