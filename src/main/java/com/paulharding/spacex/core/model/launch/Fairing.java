package com.paulharding.spacex.core.model.launch;

public class Fairing {

    private final boolean isReused;
    private final boolean isRecoveryAttempt;
    private final boolean isRecovered;
    private final String ship;

    public Fairing(boolean isReused, boolean isRecoveryAttempt, boolean isRecovered, String ship) {
        this.isReused = isReused;
        this.isRecoveryAttempt = isRecoveryAttempt;
        this.isRecovered = isRecovered;
        this.ship = ship;
    }

    public boolean isReused() {
        return isReused;
    }

    public boolean isRecoveryAttempt() {
        return isRecoveryAttempt;
    }

    public boolean isRecovered() {
        return isRecovered;
    }

    public String getShip() {
        return ship;
    }

}
