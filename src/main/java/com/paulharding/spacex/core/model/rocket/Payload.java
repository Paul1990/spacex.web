package com.paulharding.spacex.core.model.rocket;

public class Payload {

    private final String option1;
    private final String option2;
    private final CompositeFairing compositeFairing;

    public Payload(String option1, String option2, CompositeFairing compositeFairing) {
        this.option1 = option1;
        this.option2 = option2;
        this.compositeFairing = compositeFairing;
    }

    public String getOption1() {
        return option1;
    }

    public String getOption2() {
        return option2;
    }

    public CompositeFairing getCompositeFairing() {
        return compositeFairing;
    }

}
