package com.paulharding.spacex.core.gateway;

import com.paulharding.spacex.core.model.rocket.Rocket;

import java.util.List;

public interface RocketGateway {

    /**
     * Retrieves all of the rockets
     *
     * @return a List of all of the rockets
     * @throws RocketAccessException if there is an error accessing the rocket data
     */
    List<Rocket> retrieveAll();

    class RocketAccessException extends RuntimeException {

        public RocketAccessException() {}

        public RocketAccessException(String message) {
            super(message);
        }

    }

}
