package com.paulharding.spacex.core.gateway;

import com.paulharding.spacex.core.model.launch.Launch;

import java.util.List;

public interface LaunchGateway {

    /**
     * Retrieve a List of launches associated with the given rocket
     *
     * @param rocketId the ID of the rocket to retrieve launches for
     * @return a List of the given rocket's launches
     * @throws LaunchAccessException if there is an error retrieving the launches
     */
    List<Launch> retrieveRocketLaunches(String rocketId);

    class LaunchAccessException extends RuntimeException {

        public LaunchAccessException(String message) {
            super(message);
        }

    }

}
