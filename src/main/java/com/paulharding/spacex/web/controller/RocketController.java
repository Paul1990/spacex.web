package com.paulharding.spacex.web.controller;

import com.paulharding.spacex.core.interactor.RetrieveRocketInteractor;
import com.paulharding.spacex.core.model.rocket.Rocket;
import com.paulharding.spacex.core.gateway.RocketGateway;
import com.paulharding.spacex.web.model.RetrieveRocketResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;

@Controller
@RequestMapping(value = "/rocket")
public class RocketController {

    private final RetrieveRocketInteractor interactor;

    public RocketController(RetrieveRocketInteractor interactor) {
        this.interactor = interactor;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public RetrieveRocketResponse retrieveAll() {

        return safeRetrieveAll();

    }

    private RetrieveRocketResponse safeRetrieveAll() {

        try {

            List<Rocket> rockets = interactor.retrieveAll();
            return new RetrieveRocketResponse(rockets, new ArrayList<>());

        } catch (RocketGateway.RocketAccessException ex) {

            List<String> errors = singletonList("Error accessing rocket data");
            return new RetrieveRocketResponse(new ArrayList<>(), errors);

        }

    }

}
