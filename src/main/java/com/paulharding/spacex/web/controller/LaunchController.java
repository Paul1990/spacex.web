package com.paulharding.spacex.web.controller;

import com.paulharding.spacex.core.interactor.RetrieveLaunchInteractor;
import com.paulharding.spacex.core.model.launch.Launch;
import com.paulharding.spacex.web.model.RetrieveLaunchResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;

@Controller
@RequestMapping(value = "/launch")
public class LaunchController {

    private final RetrieveLaunchInteractor interactor;

    public LaunchController(RetrieveLaunchInteractor interactor) {
        this.interactor = interactor;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/{rocketId}", method = RequestMethod.GET)
    @ResponseBody
    public RetrieveLaunchResponse retrieveRocketLaunches(@PathVariable("rocketId") String rocketId) {

        return safeRetrieveRocketLaunches(rocketId);

    }

    private RetrieveLaunchResponse safeRetrieveRocketLaunches(String rocketId) {

        try {

            List<Launch> launches = interactor.retrieveRocketLaunches(rocketId);
            return new RetrieveLaunchResponse(launches, new ArrayList<>());

        } catch (Exception e) {

            List<String> errors = singletonList("Error accessing launch data");
            return new RetrieveLaunchResponse(new ArrayList<>(), errors);

        }

    }

}
