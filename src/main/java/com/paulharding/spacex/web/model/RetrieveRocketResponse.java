package com.paulharding.spacex.web.model;

import com.paulharding.spacex.core.model.rocket.Rocket;

import java.util.List;

public class RetrieveRocketResponse {

    private final List<Rocket> rockets;
    private final List<String> errors;

    public RetrieveRocketResponse(List<Rocket> rockets, List<String> errors) {
        this.rockets = rockets;
        this.errors = errors;
    }

    public List<Rocket> getRockets() {
        return rockets;
    }

    public List<String> getErrors() {
        return errors;
    }

}
