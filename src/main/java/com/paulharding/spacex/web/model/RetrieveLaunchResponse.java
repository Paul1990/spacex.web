package com.paulharding.spacex.web.model;

import com.paulharding.spacex.core.model.launch.Launch;

import java.util.List;

public class RetrieveLaunchResponse {

    private final List<Launch> launches;
    private final List<String> errors;

    public RetrieveLaunchResponse(List<Launch> launches, List<String> errors) {
        this.launches = launches;
        this.errors = errors;
    }

    public List<Launch> getLaunches() {
        return launches;
    }

    public List<String> getErrors() {
        return errors;
    }

}
