<%@ page contentType="text/html;charset=UTF-8" %>

<html>

<head>
    <title>SpaceX</title>
    <base href="${pageContext.request.contextPath}">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styles.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>

    <app-root></app-root>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/runtime.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/es2015-polyfills.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/polyfills.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/main.js"></script>

</body>

</html>
