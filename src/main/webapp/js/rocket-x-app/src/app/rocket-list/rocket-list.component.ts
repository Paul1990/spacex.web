import { Component, OnDestroy, OnInit } from '@angular/core';
import { RocketService } from '../service/rocket.service';
import { Subscription } from 'rxjs';
import DateTimeFormatOptions = Intl.DateTimeFormatOptions;

@Component({
	selector: 'app-rocket-list',
	templateUrl: './rocket-list.component.html',
	styleUrls: ['./rocket-list.component.css']
})
export class RocketListComponent implements OnInit, OnDestroy {

	rockets: Rocket[] = [];
	rocketSubscription: Subscription;

	currentSortProperty: string;
	currentSortDirection: string;

	constructor(private rocketService: RocketService) { }

	private static getCompareFunction(propertyName: string): (a: Rocket, b: Rocket) => number {

		switch (propertyName) {

			case 'name':
				return RocketListComponent.compareName;

			case 'firstFlight':
				return RocketListComponent.compareFirstFlight;

			case 'country':
				return RocketListComponent.compareCountry;

			case 'costPerLaunch':
				return RocketListComponent.compareCostPerLaunch;

			case 'active':
				return RocketListComponent.compareActive;

			case 'successRate':
				return RocketListComponent.compareSuccessRate;

			default:
				return RocketListComponent.compareName;

		}

	}

	private static compareName(a: Rocket, b: Rocket): number {

		if (a.rocketName > b.rocketName)
			return 1;

		else if (b.rocketName > a.rocketName)
			return -1;

		else return 0;

	}

	private static compareFirstFlight(a: Rocket, b: Rocket): number {

		const aDate: Date = new Date(a.firstFlight);
		const bDate: Date = new Date(b.firstFlight);

		if (aDate > bDate)
			return 1;

		else if (bDate > aDate)
			return -1;

		else return 0;

	}

	private static compareCountry(a: Rocket, b: Rocket): number {

		if (a.country > b.country)
			return 1;

		else if (b.country > a.country)
			return -1;

		else return 0;

	}

	private static compareCostPerLaunch(a: Rocket, b: Rocket): number {

		if (a.costPerLaunch > b.costPerLaunch)
			return 1;

		else if (b.costPerLaunch > a.costPerLaunch)
			return -1;

		else return 0;

	}

	private static compareActive(a: Rocket, b: Rocket): number {

		const aText: string = a.active ? 'Yes' : 'No';
		const bText: string = b.active ? 'Yes' : 'No';

		if (aText > bText)
			return 1;

		else if (bText > aText)
			return -1;

		else return 0;

	}

	private static compareSuccessRate(a: Rocket, b: Rocket): number {

		if (a.successRatePercentage > b.successRatePercentage)
			return 1;

		else if (b.successRatePercentage > a.successRatePercentage)
			return -1;

		else return 0;

	}

	private updateSort(newSortProperty: string): void {

		if (this.currentSortProperty === newSortProperty) {

			if (this.currentSortDirection === 'asc') {

				this.rockets.reverse();
				this.currentSortDirection = 'desc';

			} else {

				this.currentSortDirection = 'asc';

			}

		} else {

			this.currentSortDirection = 'asc';

		}

		this.currentSortProperty = newSortProperty;

	}

	ngOnInit(): void {
		this.retrieveRockets();
		this.sort('name');
	}

	ngOnDestroy(): void {
		this.rocketSubscription.unsubscribe();
	}

	formatDate(date: number): string {
		const options: DateTimeFormatOptions = { month: 'long', day: 'numeric', year: 'numeric' };
		return new Date(date).toLocaleDateString('en-us', options);
	}

	sort(propertyName: string): void {

		const compareFunc = RocketListComponent.getCompareFunction(propertyName);
		this.rockets.sort(compareFunc);
		this.updateSort(propertyName);

	}

	private retrieveRockets(): void {
		this.rocketSubscription = this.rocketService.retrieveRockets()
			.subscribe(response => this.rockets = response.rockets);
	}

}
