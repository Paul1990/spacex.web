import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RetrieveLaunchResponse } from '../model/retrieve-launch-response.model';

@Injectable({
	providedIn: 'root'
})
export class LaunchService {

	constructor(private httpClient: HttpClient) { }

	private static getAppBaseUrl(): string {
		return document.getElementsByTagName('base')[0].href;
	}

	retrieveRocketLaunches(rocketId: string): Observable<RetrieveLaunchResponse> {
		const appUrl = LaunchService.getAppBaseUrl();
		const requestUrl: string = appUrl + '/launch/' + rocketId;
		return this.httpClient.get<RetrieveLaunchResponse>(requestUrl);
	}

}
