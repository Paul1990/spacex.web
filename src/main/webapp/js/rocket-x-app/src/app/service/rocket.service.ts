import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RocketService {

	constructor(private httpClient: HttpClient) { }

	private static getAppBaseUrl(): string {
		return document.getElementsByTagName('base')[0].href;
	}

	retrieveRockets(): Observable<RetrieveRocketResponse> {
		const baseUrl = RocketService.getAppBaseUrl();
		const requestUrl = baseUrl + '/rocket/all';

		return this.httpClient.get<RetrieveRocketResponse>(requestUrl);
	}

}
