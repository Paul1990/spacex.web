import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RocketListComponent } from './rocket-list/rocket-list.component';
import { LaunchListComponent } from './launch-list/launch-list.component';

const routes: Routes = [
	{ path: '', component: RocketListComponent },
	{ path: 'launches/:rocketId', component: LaunchListComponent }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule { }
