import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RocketListComponent } from './rocket-list/rocket-list.component';
import { LaunchListComponent } from './launch-list/launch-list.component';

@NgModule({
	declarations: [
		AppComponent,
		RocketListComponent,
		LaunchListComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
