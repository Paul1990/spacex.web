import { Component, OnDestroy, OnInit } from '@angular/core';
import { Launch } from '../model/launch/launch.model';
import { LaunchService } from '../service/launch.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-launch-list',
	templateUrl: './launch-list.component.html',
	styleUrls: ['./launch-list.component.css']
})
export class LaunchListComponent implements OnInit, OnDestroy {

	launches: Launch[] = [];
	loadErrors: string[] = [];
	rocketName: string;

	currentSortProperty: string;
	currentSortDirection: string;

	private launchSubscription: Subscription;

	constructor(private route: ActivatedRoute, private launchService: LaunchService) { }

	private static getCompareFunction(propertyName: string): (a: Launch, b: Launch) => number {

		switch (propertyName) {

			case 'flightNumber':
				return LaunchListComponent.compareFlightNumber;

			case 'missionName':
				return LaunchListComponent.compareMissionName;

			case 'launchYear':
				return LaunchListComponent.compareLaunchYear;

			case 'launchSite':
				return LaunchListComponent.compareLaunchSite;

			default:
				return LaunchListComponent.compareFlightNumber;

		}

	}

	private static compareFlightNumber(a: Launch, b: Launch): number {

		if (a.flightNumber > b.flightNumber)
			return 1;

		else if (b.flightNumber > a.flightNumber)
			return -1;

		else return 0;

	}

	private static compareMissionName(a: Launch, b: Launch): number {

		if (a.missionName > b.missionName)
			return 1;

		else if (b.missionName > a.missionName)
			return -1;

		else return 0;

	}

	private static compareLaunchYear(a: Launch, b: Launch): number {

		if (a.launchYear > b.launchYear)
			return 1;

		else if (b.launchYear > a.launchYear)
			return -1;

		else return 0;

	}

	private static compareLaunchSite(a: Launch, b: Launch): number {

		if (a.launchSite > b.launchSite)
			return 1;

		else if (b.launchSite > a.launchSite)
			return -1;

		else return 0;

	}

	private updateSort(newSortProperty: string): void {

		if (this.currentSortProperty === newSortProperty) {

			if (this.currentSortDirection === 'asc') {

				this.launches.reverse();
				this.currentSortDirection = 'desc';

			} else {

				this.currentSortDirection = 'asc';

			}

		} else {

			this.currentSortDirection = 'asc';

		}

		this.currentSortProperty = newSortProperty;

	}

	private retrieveLaunches(rocketId: string): void {
		this.launchSubscription = this.launchService.retrieveRocketLaunches(rocketId)
			.subscribe((response) => {
				this.launches = response.launches;
				this.loadErrors = response.errors;
				this.rocketName = this.launches[0].rocket.name;
			});
	}

	ngOnInit(): void {
		const rocketId: string = this.route.snapshot.paramMap.get('rocketId');
		this.retrieveLaunches(rocketId);
		this.sort('flightNumber');
	}

	ngOnDestroy(): void {
		this.launchSubscription.unsubscribe();
	}

	sort(propertyName: string): void {

		const compareFunc = LaunchListComponent.getCompareFunction(propertyName);
		this.launches.sort(compareFunc);
		this.updateSort(propertyName);

	}

}
