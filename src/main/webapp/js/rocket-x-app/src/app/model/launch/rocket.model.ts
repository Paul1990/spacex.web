import { FirstStage } from './first-stage.model';
import { SecondStage } from './second-stage.model';
import { Fairing } from './fairing.model';

export interface Rocket {
	id: string;
	name: string;
	type: string;
	firstStage: FirstStage;
	secondStage: SecondStage;
	fairings: Fairing;
}
