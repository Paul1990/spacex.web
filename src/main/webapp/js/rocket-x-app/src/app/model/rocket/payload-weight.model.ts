interface PayloadWeight {
  id: string;
  name: string;
  mass: Mass;
}
