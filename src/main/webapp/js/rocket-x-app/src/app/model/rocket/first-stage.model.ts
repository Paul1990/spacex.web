interface FirstStage {
  reusable: boolean;
  numberOfEngines: number;
  tonsOfFuel: number;
  numberOfCores: number;
  secondsOfBurnTime: number;
  seaLevelThrust: Force;
  vacuumThrust: Force;
}
