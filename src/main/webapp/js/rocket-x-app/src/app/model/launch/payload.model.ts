import { Orbit } from './orbit.model';

export interface Payload {
	id: string;
	noradId: number[];
	reused: boolean;
	customers: string[];
	nationality: string;
	manufacturer: string;
	type: string;
	massInKilograms: number;
	massInPounds: number;
	orbit: string;
	orbitParam: Orbit;
}
