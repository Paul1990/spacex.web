interface Rocket {
  id: number;
  active: boolean;
  numberOfStages: number;
  numberOfBoosters: number;
  costPerLaunch: number;
  successRatePercentage: number;
  firstFlight: number;
  country: string;
  company: string;
  height: Distance;
  diameter: Distance;
  mass: Mass;
  payloadWeight: PayloadWeight[];
  firstStage: FirstStage;
  secondStage: SecondStage;
  engine: Engine;
  landingLeg: LandingLeg;
  imageUrls: string[];
  wikipediaUrl: string;
  description: string;
  rocketId: string;
  rocketName: string;
  rocketType: string;
}
