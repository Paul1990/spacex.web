import { Launch } from './launch/launch.model';

export interface RetrieveLaunchResponse {
	errors: string[];
	launches: Launch[];
}
