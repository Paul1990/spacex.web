import { Telemetry } from './telemetry.model';
import { LaunchSite } from './launch-site.model';
import { LaunchFailureDetail } from './launch-failure-detail.model';
import { Timeline } from './timeline.model';
import { LaunchLink } from './launch-link.model';
import { Rocket } from './rocket.model';

export interface Launch {
	flightNumber: number;
	missionName: string;
	missionIds: string[];
	upcoming: boolean;
	launchYear: string;
	unixLaunchDate: number;
	utcLaunchDate: string;
	localLaunchDate: string;
	tentative: boolean;
	tentativeMaxPrecision: string;
	tbd: boolean;
	launchWindow: number;
	rocket: Rocket;
	ships: string[];
	telemetry: Telemetry;
	launchSite: LaunchSite;
	launchSuccessful: boolean;
	launchFailureDetail: LaunchFailureDetail;
	links: LaunchLink;
	details: string;
	utcStaticFireDate: string;
	unixStaticFireDate: string;
	timeline: Timeline;
}
