export interface Core {
	coreSerial: string;
	flight: string;
	block: number;
	gridfins: boolean;
	legs: boolean;
	reused: boolean;
	landSuccessful: boolean;
	landingIntent: boolean;
	landingType: string;
	landingVehicle: string;
}
