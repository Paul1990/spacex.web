export interface Orbit {
	referenceSystem: string;
	regime: string;
	longitude: number;
	semiMajorAxisKilometers: number;
	eccentricity: number;
	periapsisKilometers: number;
	apoapsisKilometers: number;
	inclinationDegrees: number;
	periodMinimum: number;
	lifeSpanInYears: number;
	epoch: string;
	meanMotion: number;
	raan: number;
	argOfPericenter: number;
	meanAnomaly: number;
}
