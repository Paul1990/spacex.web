export interface LaunchLink {
	missionPatch: string;
	missionPatchSmall: string;
	redditCampaign: string;
	redditLaunch: string;
	redditRecovery: string;
	redditMedia: string;
	pressKit: string;
	articleLink: string;
	wikipedia: string;
	videoLink: string;
	youtubeId: string;
	flickrImages: string[];
}
