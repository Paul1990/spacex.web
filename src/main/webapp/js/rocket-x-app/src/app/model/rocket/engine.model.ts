interface Engine {
  number: number;
  type: string;
  version: string;
  layout: string;
  maxEngineLoss: number;
  propellant1: string;
  propellant2: string;
  seaLevelThrust: Force;
  vacuumThrust: Force;
  thrustToWeight: number;
}
