interface RetrieveRocketResponse {
  errors: string[];
  rockets: Rocket[];
}
