export interface LaunchSite {
	id: string;
	name: string;
	longName: string;
}
