interface SecondStage {
  reusable: boolean;
  numberOfEngines: number;
  tonsOfFuel: number;
  secondsOfBurnTime: number;
  thrust: Force;
  payload: Payload;
}
