export interface LaunchFailureDetail {
	time: number;
	altitude: number;
	reason: string;
}
