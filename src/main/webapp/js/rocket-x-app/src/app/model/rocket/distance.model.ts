interface Distance {
  meters: number;
  feet: number;
}
