export interface Fairing {
	reused: boolean;
	recoveryAttempt: boolean;
	recovered: boolean;
	ship: string;
}
