interface Payload {
  option1: string;
  option2: string;
  compositeFairing: CompositeFairing;
}
