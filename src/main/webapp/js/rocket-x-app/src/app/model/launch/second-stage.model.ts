import { Payload } from './payload.model';

export interface SecondStage {
	block: number;
	payloads: Payload[];
}
