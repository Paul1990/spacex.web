**Running the app:**
- Clone the project
- Build the project
- Deploy the WAR to a server (tested on Tomcat 8.0.37)
Note: The front end is written in Angular, but there is no need to run ```ng serve```; the production build files are included in ```index.jsp```
Note: If you do run ```ng serve```, the request urls in ```launch.service.ts``` and ```rocket.service.ts``` would need to be changed to point to your local instance of the Java webapp

**Design Notes**
In the Java app, there are 3 main packages

- core
    - This package contains the interfaces/classes that would be needed regardless of how the app was deployed (ie web, console, desktop, etc)
    - It does not depend on the other packages
    
- gateway
    - This package is a data access "plug-in" to ```core```; it implements ```RocketGateway``` and ```LaunchGateway```
    - If another implementation of the gateways was desired (say an implementation that caches the results of the API call), then that implementation could be placed here, and it could "plug in" to ```core``` as well
    - This package depends on ```core```
   
- web
    - This is the Java code that interacts with the UI; it contains the Spring MVC controllers
    - It depends on ```core```